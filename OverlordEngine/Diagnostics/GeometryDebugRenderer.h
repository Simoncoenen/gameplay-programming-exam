#pragma once

struct VertexPosCol;
struct GameContext;

class GeometryDebugRenderer
{
public:
	static void InitRenderer(ID3D11Device *pDevice, UINT bufferSize = 100);
	static void ToggleDebugRenderer();
	static void Release();

	//RENDERING
	static void Draw(const GameContext& gameContext);
	static void DrawQuad(XMFLOAT3 pos, float size, XMFLOAT4 color = (XMFLOAT4)Colors::Red);

private:
	GeometryDebugRenderer(void);
	~GeometryDebugRenderer(void);

	//RENDERING
	static void CreateVertexBuffer(ID3D11Device *pDevice);
	static XMFLOAT4 ConvertPxColor(PxU32 color);
	static ID3DX11Effect* m_pEffect;
	static ID3DX11EffectTechnique* m_pTechnique;
	static UINT m_BufferSize;
	static ID3D11InputLayout *m_pInputLayout;
	static ID3D11Buffer *m_pVertexBuffer;
	static ID3DX11EffectMatrixVariable *m_pWvpVariable;
	static vector<VertexPosCol> m_Vertices;
	static bool m_RendererEnabled;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	GeometryDebugRenderer(const GeometryDebugRenderer& t);
	GeometryDebugRenderer& operator=(const GeometryDebugRenderer& t);
};

