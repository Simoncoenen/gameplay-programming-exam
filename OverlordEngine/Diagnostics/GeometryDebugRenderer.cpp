//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "GeometryDebugRenderer.h"
#include "../Base/GeneralStructs.h"
#include "Logger.h"
#include "../Content/ContentManager.h"

ID3DX11Effect* GeometryDebugRenderer::m_pEffect = nullptr;
ID3DX11EffectTechnique* GeometryDebugRenderer::m_pTechnique = nullptr;
UINT GeometryDebugRenderer::m_BufferSize = 6 * 100;
ID3D11Buffer* GeometryDebugRenderer::m_pVertexBuffer = nullptr;
ID3D11InputLayout* GeometryDebugRenderer::m_pInputLayout = nullptr;
ID3DX11EffectMatrixVariable* GeometryDebugRenderer::m_pWvpVariable = nullptr;
bool GeometryDebugRenderer::m_RendererEnabled = true;
std::vector<VertexPosCol> GeometryDebugRenderer::m_Vertices;

GeometryDebugRenderer::GeometryDebugRenderer(void)
{
}

GeometryDebugRenderer::~GeometryDebugRenderer(void)
{
}

void GeometryDebugRenderer::Release()
{
	SafeRelease(m_pInputLayout);
	SafeRelease(m_pVertexBuffer);
}

void GeometryDebugRenderer::InitRenderer(ID3D11Device *pDevice, UINT bufferSize)
{
	m_BufferSize = bufferSize;
	m_pEffect = ContentManager::Load<ID3DX11Effect>(L"./Resources/Effects/DebugRenderer.fx");
	m_pTechnique = m_pEffect->GetTechniqueByIndex(0);
	m_pWvpVariable = m_pEffect->GetVariableBySemantic("WORLDVIEWPROJECTION")->AsMatrix();

	if(!m_pWvpVariable->IsValid())
		Logger::LogWarning(L"Debug Renderer: Invalid Shader Variable! (WVP)");

	//Input Layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
	};

	D3DX11_PASS_DESC passDesc;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&passDesc);
	pDevice->CreateInputLayout(layout, 2, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &m_pInputLayout);

	CreateVertexBuffer(pDevice);
}

void GeometryDebugRenderer::CreateVertexBuffer(ID3D11Device *pDevice)
{
	SafeRelease(m_pVertexBuffer);

	//Vertexbuffer
	D3D11_BUFFER_DESC buffDesc;
	buffDesc.Usage = D3D11_USAGE_DYNAMIC;
	buffDesc.ByteWidth = sizeof(VertexPosCol) * (m_BufferSize);
	buffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	buffDesc.MiscFlags = 0;
	HRESULT hr =pDevice->CreateBuffer(&buffDesc, nullptr, &m_pVertexBuffer);
	Logger::LogHResult(hr, L"GeometryDebugRenderer::CreateVertexBuffer()...");
}

void GeometryDebugRenderer::ToggleDebugRenderer()
{
	m_RendererEnabled = !m_RendererEnabled;
}

XMFLOAT4 GeometryDebugRenderer::ConvertPxColor(PxU32 color)
{
	//TODO: Check performance, Bitshift+divide vs switch
	switch(color)
	{
		case 0xFF000000:
			return (XMFLOAT4)Colors::Black;
		case 0xFFFF0000:
			return (XMFLOAT4)Colors::Red;
		case 0xFF00FF00:
			return (XMFLOAT4)Colors::Green;
		case 0xFF0000FF:
			return (XMFLOAT4)Colors::Blue;
		case 0xFFFFFF00:
			return (XMFLOAT4)Colors::Yellow;
		case 0xFFFF00FF:
			return (XMFLOAT4)Colors::Magenta;
		case 0xFF00FFFF:
			return (XMFLOAT4)Colors::Cyan;
		case 0xFFFFFFFF:
			return (XMFLOAT4)Colors::White;
		case 0xFF808080:
			return (XMFLOAT4)Colors::Gray;
		case 0x88880000:
			return (XMFLOAT4)Colors::DarkRed;
		case 0x88008800:
			return (XMFLOAT4)Colors::DarkGreen;
		case 0x88000088:
			return (XMFLOAT4)Colors::DarkBlue;
		default:
			return (XMFLOAT4)Colors::Black;
	}
}

void GeometryDebugRenderer::Draw(const GameContext& gameContext)
{
	if(!m_RendererEnabled)
		return;

	UINT size = m_Vertices.size();

	if(size > m_BufferSize)
	{
		Logger::LogInfo(L"GeometryDebugRenderer::Draw() > Increasing Vertexbuffer Size!");
		m_BufferSize = size;
		CreateVertexBuffer(gameContext.pDevice);
	}

	auto pDevContext = gameContext.pDeviceContext;

	if(size>0)
	{
		//Map Vertex Data
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		pDevContext->Map(m_pVertexBuffer, 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &mappedResource);
		memcpy(mappedResource.pData, m_Vertices.data(), sizeof(VertexPosCol) * size);
		pDevContext->Unmap(m_pVertexBuffer, 0);
	}

	//Set Render Pipeline
	pDevContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	UINT stride = sizeof(VertexPosCol);
	UINT offset = 0;
	pDevContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	pDevContext->IASetInputLayout(m_pInputLayout);

	auto viewProj = gameContext.pCamera->GetViewProjection();
	XMMATRIX wvp = XMMatrixIdentity() * XMLoadFloat4x4(&viewProj);
	XMFLOAT4X4 wvpConverted;
	XMStoreFloat4x4( &wvpConverted, wvp);

	m_pWvpVariable->SetMatrix(reinterpret_cast<float*>(&wvpConverted));

	D3DX11_TECHNIQUE_DESC techDesc;
	m_pTechnique->GetDesc(&techDesc);
	for(UINT i = 0; i < techDesc.Passes; ++i)
	{
		m_pTechnique->GetPassByIndex(i)->Apply(0, pDevContext);
		pDevContext->Draw(size, 0);
	}
	
	m_Vertices.clear();
}

void GeometryDebugRenderer::DrawQuad(XMFLOAT3 pos, float size, XMFLOAT4 color)
{
	VertexPosCol leftTop, rightTop, leftBottom, rightBottom;
	leftTop.Position = rightTop.Position = leftBottom.Position = rightBottom.Position = pos;
	leftTop.Color = rightTop.Color = leftBottom.Color = rightBottom.Color = color;
	leftTop.Position.x -= size / 2.0f;
	leftTop.Position.z += size / 2.0f;
	rightTop.Position.x += size / 2.0f;
	rightTop.Position.z += size / 2.0f;
	leftBottom.Position.x -= size / 2.0f;
	leftBottom.Position.z -= size / 2.0f;
	rightBottom.Position.x += size / 2.0f;
	rightBottom.Position.z -= size / 2.0f;

	m_Vertices.push_back(leftTop);
	m_Vertices.push_back(rightTop);
	m_Vertices.push_back(rightBottom);

	m_Vertices.push_back(leftTop);
	m_Vertices.push_back(rightBottom);
	m_Vertices.push_back(leftBottom);
}