#pragma once

inline XMFLOAT3 QuaternionToEuler(XMFLOAT4 q)
{
	XMFLOAT3 euler;

	euler.y = atan2f(2.f * q.x * q.w + 2.f * q.y * q.z, 1.f - 2.f * ((q.z*q.z) + (q.w*q.w)));     // Yaw 
	euler.x = asinf(2.f * (q.x * q.z - q.w * q.y));                             // Pitch 
	euler.z = atan2f(2.f * q.x * q.y + 2.f * q.z * q.w, 1.f - 2.f * ((q.y*q.y) + (q.z*q.z)));      // Roll 

	return euler;
}

inline bool XMFloat4Equals(const XMFLOAT4& a, const XMFLOAT4& b)
{
	return (a.x == b.x) && (a.y == b.y) && (a.z == b.z) && (a.w == b.w);
}

inline bool XMFloat3Equals(const XMFLOAT3& a, const XMFLOAT3& b)
{
	return (a.x == b.x) && (a.y == b.y) && (a.z == b.z);
}

inline bool XMFloat2Equals(const XMFLOAT2& a, const XMFLOAT2& b)
{
	return (a.x == b.x) && (a.y == b.y);
}

inline float randF(float min, float max)
{
	float random = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
	float diff = max - min;
	float r = random * diff;
	return min + r;
}

inline int randI(int min, int max)
{
	return min + rand() % (max - min + 1);
}

inline PxVec3 MoveTowards(const PxVec3& current, const PxVec3& target, float maxDistanceDelta)
{
	PxVec3 a = target - current;
	const PxReal magnitude = a.magnitude();
	if (magnitude <= maxDistanceDelta || magnitude == 0)
		return target;
	return current + a / magnitude * maxDistanceDelta;
}

inline float DistanceXZ(const XMFLOAT3& a, const XMFLOAT3& b)
{
	return sqrt(pow(a.x - b.x, 2) + pow(a.z - b.z, 2));
}

inline float DistanceXZ(const PxVec3& a, const PxVec3& b)
{
	return sqrt(pow(a.x - b.x, 2) + pow(a.z - b.z, 2));
}

inline XMVECTOR CreateLookRotation(XMVECTOR vDirection, XMVECTOR upDir)
{
	XMVector3Normalize(-vDirection);
	XMVECTOR xmRight = XMVector3Cross(upDir, -vDirection);
	XMVECTOR xmUp = XMVector3Cross(xmRight, vDirection);
	XMFLOAT3 forward, up, right;
	XMStoreFloat3(&forward, -vDirection);
	XMStoreFloat3(&up, xmRight);
	XMStoreFloat3(&right, xmUp);

	XMFLOAT3X3 matrix;
	matrix._11 = up.x;
	matrix._12 = up.y;
	matrix._13 = up.z;
	matrix._21 = right.x;
	matrix._22 = right.y;
	matrix._23 = right.z;
	matrix._31 = forward.x;
	matrix._32 = forward.y;
	matrix._33 = forward.z;

	XMVECTOR quat, scale, trans;

	XMMATRIX m = XMLoadFloat3x3(&matrix);

	XMMatrixDecompose(&scale, &quat, &trans, m);
	return quat;
}

inline float Lerp(const float a, const float b, const float value)
{
	return (1 - value)*a + value * b;
}

inline float InverseLerp(const float min, const float max, const float value)
{
	return (value - min) / (max - min);
}