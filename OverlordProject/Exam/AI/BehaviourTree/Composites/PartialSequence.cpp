#include "stdafx.h"
#include "Composites.h"

BehaviourState PartialSequence::Execute()
{
	while (m_CurrentBehaviourIndex < static_cast<int>(m_ChildrenBehaviours.size()))
	{
		m_CurrentState = m_ChildrenBehaviours[m_CurrentBehaviourIndex]->Execute();
		switch (m_CurrentState)
		{
		case BehaviourState::FAILURE:
			m_CurrentBehaviourIndex = 0;
			return m_CurrentState;
		case BehaviourState::SUCCESS:
			++m_CurrentBehaviourIndex;
			return m_CurrentState = BehaviourState::RUNNING;
		case BehaviourState::RUNNING:
			return m_CurrentState;
		default:
			continue;
		}
	}
	m_CurrentBehaviourIndex = 0;
	return m_CurrentState = BehaviourState::SUCCESS;
}