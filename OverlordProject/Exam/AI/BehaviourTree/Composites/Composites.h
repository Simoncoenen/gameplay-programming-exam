#include "../Constants.h" 

class BehaviourNode
{
public:
	BehaviourNode(){}

	virtual ~BehaviourNode()
	{
		for (BehaviourNode* node : m_ChildrenBehaviours)
			SafeDelete(node);
	}
	virtual BehaviourState Execute() = 0;

	void AddChild(BehaviourNode* child)
	{
		m_ChildrenBehaviours.push_back(child);
	}

protected:
	BehaviourState m_CurrentState = BehaviourState::RUNNING;
	vector<BehaviourNode*> m_ChildrenBehaviours;
};

class Sequence : public BehaviourNode
{
public:
	Sequence() {}
	~Sequence() {}

	BehaviourState Execute() override;
};

class Selector : public BehaviourNode
{
public:
	Selector() {}
	~Selector() {}

	BehaviourState Execute() override;
};

class PartialSequence : public BehaviourNode
{
public:
	PartialSequence() {}
	~PartialSequence() {}

	BehaviourState Execute() override;

private:
	int m_CurrentBehaviourIndex = 0;
};
