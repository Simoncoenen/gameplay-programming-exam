#include "stdafx.h"
#include "SteeringZombie.h"
#include <Exam/Helpers/Enumerations.h>
#include <Components/Components.h>
#include <Exam/AI/BehaviourTree/BehaviourAction.h>
#include <Exam/Prefabs/Players/Player.h>
#include <Graphics/ModelAnimator.h>

SteeringZombie::SteeringZombie()
{
}

SteeringZombie::~SteeringZombie()
{
}

void SteeringZombie::Init(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//Seperate gameobject from mesh
	GameObject* pMeshObject = new GameObject();
	m_pModelComponent = new ModelComponent(L"./Resources/Meshes/Characters/Zombie_Anim.ovm");
	m_pModelComponent->SetMaterial(MaterialIndex::mZombie);
	pMeshObject->AddComponent(m_pModelComponent);
	pMeshObject->GetTransform()->Scale(0.9f, 0.9f, 0.9f);
	pMeshObject->GetTransform()->Translate(0.0f, -m_Height, 0.0f);
	AddChild(pMeshObject);

	//Behaviour Tree
	m_Root = new Selector();

	BehaviourAction* action = new BehaviourAction(bind(&SteeringZombie::Die, this));
	m_Root->AddChild(action);

	Sequence* pSequence2 = new Sequence();
	pSequence2->AddChild(new BehaviourAction(bind(&SteeringZombie::IsInRange, this)));
	pSequence2->AddChild(new BehaviourAction(bind(&SteeringZombie::IsInFront, this)));
	pSequence2->AddChild(new BehaviourAction(bind(&SteeringZombie::Attack, this)));
	m_Root->AddChild(pSequence2);

	Sequence* pSequence3 = new Sequence();

	pSequence3->AddChild(new BehaviourAction(bind(&SteeringZombie::MoveToTarget, this)));
	m_Root->AddChild(pSequence3);
	SetComposite(m_Root);
}

BehaviourState SteeringZombie::Attack()
{
	m_pAnimator->SetAnimation(L"Attack");
	m_pAnimator->Play();
	if (m_pAnimator->AtTheEnd())
	{
		m_pTarget->ReceiveDamage(m_Damage);
	}
	return BehaviourState::RUNNING;
}

BehaviourState SteeringZombie::IsInFront()
{
	XMFLOAT3 fwd = GetTransform()->GetForward();
	XMFLOAT3 pos = GetTransform()->GetWorldPosition();
	XMFLOAT3 targetPos = m_pTarget->GetTransform()->GetWorldPosition();
	XMFLOAT3 dir;
	dir.x = targetPos.x - pos.x;
	dir.y = targetPos.y - pos.y;
	dir.z = targetPos.z - pos.z;
	XMVECTOR xmDir = XMLoadFloat3(&dir);
	xmDir = XMVector3Normalize(xmDir);
	XMVECTOR xmFwd = XMLoadFloat3(&fwd);
	XMVECTOR xmDot = XMVector3Dot(xmDir, xmFwd);
	float dot;
	XMStoreFloat(&dot, xmDot);

	if (dot + 1 < 0.1f)
		return BehaviourState::SUCCESS;

	return BehaviourState::FAILURE;
}