#include "stdafx.h"
#include "SteeringEnemy.h"
#include <Components/ControllerComponent.h>
#include <Physx/PhysxManager.h>
#include "Components/Components.h"
#include <Exam/Helpers/Enumerations.h>
#include "../../BehaviourTree/BehaviourAction.h"
#include <Exam/AI/Pathfinding/Pathfinding.h>
#include <Exam/AI/Pathfinding/Grid.h>
#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/Prefabs/Players/Player.h>
#include <Graphics/ModelAnimator.h>
#include <Diagnostics/DebugRenderer.h>
#include <Base/SoundManager.h>
#include <Exam/Managers/DecalManager.h>

vector<SteeringEnemy*> SteeringEnemy::m_Friends;

SteeringEnemy::SteeringEnemy()
{
	m_Friends.push_back(this);
}

SteeringEnemy::~SteeringEnemy()
{
	auto it = find(m_Friends.begin(), m_Friends.end(), this);
	if (it != m_Friends.end())
		m_Friends.erase(it);
	else
		Logger::LogWarning(L"SteeringEnemy::~SteeringEnemy() No instance found in Friends array");
	delete m_Root;
}

void SteeringEnemy::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	SetTag(L"Enemy");
	m_pController = new ControllerComponent(PhysxManager::GetInstance()->GetDefaultMaterial(), m_Radius, m_Height);
	m_pController->SetCollisionGroup(CollisionGroup::cEnemy);
	AddComponent(m_pController);

	m_pGrid = Pathfinding::GetInstance()->GetGrid();
	Init(gameContext);
}

void SteeringEnemy::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pAnimator = m_pModelComponent->GetAnimator();
}

void SteeringEnemy::Update(const GameContext& gameContext)
{
	BehaviourTree::Update(gameContext);
}

void SteeringEnemy::Draw(const GameContext& gameContext)
{
	DebugRenderer::DrawCircle(GetTransform()->GetWorldPosition(), m_FriendRange);
}

void SteeringEnemy::Damage(int damage)
{
	m_Health -= damage;
	DecalManager::GetInstance()->Add(GetTransform()->GetWorldPosition(), DecalManager::Type::BLOOD, GetScene());
}

BehaviourState SteeringEnemy::MoveToTarget()
{
	m_pAnimator->SetAnimation(L"Walk");
	PxVec3 result = 1.5f * Seperation() + Cohesion() + FlowFieldFollow();
	result.y = 0;
	result = result.getNormalized() * m_MaxVelocity;
	result *= m_DeltaTime;

	m_CurrentVelocity = MoveTowards(m_CurrentVelocity, result, m_DeltaTime * 0.2f);

	m_pController->Move(m_CurrentVelocity);

	//Rotate in velocity direction
	XMFLOAT3 upDir(0, 1, 0);
	XMVECTOR up = XMLoadFloat3(&upDir);
	XMFLOAT3 velocity = ToXMFLOAT3(m_CurrentVelocity);
	XMVECTOR vel = XMLoadFloat3(&velocity);
	GetTransform()->Rotate(CreateLookRotation(vel, up));

	return BehaviourState::RUNNING;
}

BehaviourState SteeringEnemy::IsInRange()
{
	float distance = DistanceXZ(GetTransform()->GetWorldPosition(), m_pTarget->GetTransform()->GetWorldPosition());
	if (distance <= m_AttackRange)
		return BehaviourState::SUCCESS;
	return BehaviourState::FAILURE;
}

BehaviourState SteeringEnemy::Die()
{
	cout << m_Health << endl;
	if (m_Health <= 0)
	{
		//Check if already died so we play sound only once
		if (m_Died == false)
		{
			m_pTarget->AddKill();
			int randomSound = rand() % 2 + 1;
			stringstream stream;
			stream << "./Resources/Sounds/Enemies/ZombieDeath (" << randomSound << ").wav";
			FMOD::Sound* pSound = SoundManager::GetInstance()->LoadSound(stream.str(), FMOD_2D);
			SoundManager::GetInstance()->GetSystem()->playSound(pSound, nullptr, false, nullptr);
			RemoveComponent(m_pController);
			SafeDelete(m_pController);
			m_pAnimator->SetAnimation(L"Death");
			m_pAnimator->SetLoop(false);
			m_pAnimator->Play();
			m_Died = true;
		}
		m_DeathTimer += m_DeltaTime;
		if (m_DeathTimer >= 2.0f)
		{
			m_IsDead = true;
			return BehaviourState::SUCCESS;
		}
		return BehaviourState::RUNNING;
	}
	return BehaviourState::FAILURE;
}

PxVec3 SteeringEnemy::Seek(const PxVec3& target)
{
	PxVec3 agentPos = GetTransform()->GetWorldPositionPcVec3();
	PxVec3 steering = (target - agentPos).getNormalized();
	return steering;
}

PxVec3 SteeringEnemy::Flee(const PxVec3& target)
{
	return Seek(target) * -1.0f;
}

PxVec3 SteeringEnemy::Pursuit()
{
	PxVec3 targetVelocity = m_pTarget->GetComponent<ControllerComponent>()->GetVelocity();
	PxVec3 targetPos = m_pTarget->GetTransform()->GetWorldPositionPcVec3();
	PxVec3 agentPos = GetTransform()->GetWorldPositionPcVec3();
	float ahead = (targetPos - agentPos).magnitude() / m_MaxVelocity * 0.4f;
	PxVec3 pursuePos = m_pTarget->GetTransform()->GetWorldPositionPcVec3() + ahead * targetVelocity;
	return Seek(pursuePos);
}

PxVec3 SteeringEnemy::Avoid()
{
	PxVec3 targetVelocity = m_pTarget->GetComponent<ControllerComponent>()->GetVelocity();
	PxVec3 targetPos = m_pTarget->GetTransform()->GetWorldPositionPcVec3();
	PxVec3 agentPos = GetTransform()->GetWorldPositionPcVec3();
	float ahead = (targetPos - agentPos).magnitude() / m_MaxVelocity * 0.4f;
	PxVec3 fleePos = m_pTarget->GetTransform()->GetWorldPositionPcVec3() + ahead * targetVelocity;
	return Flee(fleePos);
}

PxVec3 SteeringEnemy::Alignment()
{
	PxVec3 steering(0.0f, 0.0f, 0.0f);

	int friendsInRange = 0;
	for(SteeringEnemy* pFriend : m_Friends)
	{
		if (pFriend == this)
			continue;
		if(DistanceXZ(pFriend->GetTransform()->GetWorldPosition(), GetTransform()->GetWorldPosition()) <= m_FriendRange)
		{
			steering += pFriend->GetComponent<ControllerComponent>()->GetVelocity();
			++friendsInRange;
		}
	}
	if (friendsInRange == 0)
		return steering;
	steering /= (float)friendsInRange;
	steering.normalize();
	return steering;
}

PxVec3 SteeringEnemy::Seperation()
{
	PxVec3 steering(0.0f, 0.0f, 0.0f);
	int friendsInRange = 0;

	PxVec3 agentPos = GetTransform()->GetWorldPositionPcVec3();
	for (SteeringEnemy* pFriend : m_Friends)
	{
		if (pFriend == this)
			continue;
		if (DistanceXZ(pFriend->GetTransform()->GetWorldPosition(), ToXMFLOAT3(agentPos)) <= m_FriendRange)
		{
			steering += pFriend->GetTransform()->GetWorldPositionPcVec3() - agentPos;
			++friendsInRange;
		}
	}
	if (friendsInRange == 0)
		return steering;
	steering /= (float)friendsInRange;
	steering *= -1;
	steering.normalize();
	return steering;
}

PxVec3 SteeringEnemy::Cohesion()
{
	PxVec3 steering(0.0f, 0.0f, 0.0f);

	int friendsInRange = 0;
	PxVec3 agentPos = GetTransform()->GetWorldPositionPcVec3();
	for (SteeringEnemy* pFriend : m_Friends)
	{
		if (pFriend == this)
			continue;
		if (DistanceXZ(pFriend->GetTransform()->GetWorldPosition(), ToXMFLOAT3(agentPos)) <= m_FriendRange)
		{
			steering += pFriend->GetTransform()->GetWorldPositionPcVec3();
			++friendsInRange;
		}
	}
	if (friendsInRange == 0)
		return steering;
	steering /= (float)friendsInRange;
	steering = steering - agentPos;
	steering.normalize();
	return steering;
}

PxVec3 SteeringEnemy::FlowFieldFollow()
{
	Node* pNode = m_pGrid->NodeFromWorldPoints(GetTransform()->GetWorldPosition());
	return pNode->GetVector();
}
