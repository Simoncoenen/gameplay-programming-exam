#pragma once
#include "SteeringEnemy.h"
class SteeringZombie : public SteeringEnemy
{
public:
	SteeringZombie();
	~SteeringZombie();

	void Init(const GameContext& gameContext);
	
private:
	BehaviourState IsInFront();
	BehaviourState Attack();
};

