#pragma once
#include "SteeringEnemy.h"
class SteeringDevil : public SteeringEnemy
{
public:
	SteeringDevil();
	~SteeringDevil();

	void Init(const GameContext& gameContext);
private:
	BehaviourState Attack();
	BehaviourState LookAtPlayer();

	GameObject* m_pFireBallSpawnSocket = nullptr;
	XMFLOAT3 m_FireBallSpawnPosition = { 0.0f, 3.0f, -7.0f };
};