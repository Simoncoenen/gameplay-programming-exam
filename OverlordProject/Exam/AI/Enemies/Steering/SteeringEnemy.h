#pragma once
#include "..\..\BehaviourTree\BehaviourTree.h"

class Player;
class ControllerComponent;
class Grid;
class ModelComponent;
class ModelAnimator;

class SteeringEnemy : public BehaviourTree
{
public:
	SteeringEnemy();
	~SteeringEnemy();

	void Initialize(const GameContext& gameContext);
	virtual void Init(const GameContext& gameContext) = 0;
	void PostInitialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void Draw(const GameContext& gameContext);

	void SetTarget(Player* pTarget) { m_pTarget = pTarget; }
	void Damage(int damage);

	void SetActive(const bool active = true) { m_Active = active; }
	bool IsDead() const { return m_IsDead; }

protected:
	//Behaviour Tree
	BehaviourNode* m_Root = nullptr;
	BehaviourState MoveToTarget();
	BehaviourState IsInRange();
	virtual BehaviourState Attack() = 0;
	BehaviourState Die();

	//Properties
	float m_MaxVelocity = 7.0f;
	bool m_IsDead = false;
	int m_Health = 10;
	float m_DeathTimer = 0.0f;
	float m_DeathAnimationTime = 0.0f;
	bool m_Active = true;
	bool m_Died = false;

	//Attacking
	float m_AttackRange = 11.0f;
	float m_AttackTimer = 0.0f;
	float m_AttackRate = 1.0f;
	int m_Damage = 20;

	//Components
	ControllerComponent* m_pController = nullptr;
	float m_Height = 7.0f;
	float m_Radius = 4.0f;
	bool m_IsAttacking = false;

	ModelComponent* m_pModelComponent = nullptr;
	ModelAnimator* m_pAnimator = nullptr;

	////////////////
	//**Steering**//
	////////////////
	//Pathfinding
	Grid* m_pGrid = nullptr;
	Player* m_pTarget = nullptr;

	float m_FriendRange = 15.0f;
	static vector<SteeringEnemy*> m_Friends;
	PxVec3 m_CurrentVelocity = {0,0,0};

	//Steering constants
	float m_PursuitAhead = 2.0f;

	//Steering methods
	PxVec3 Seek(const PxVec3& target);
	PxVec3 Flee(const PxVec3& target);
	PxVec3 Pursuit();
	PxVec3 Avoid();
	PxVec3 Alignment();
	PxVec3 Seperation();
	PxVec3 Cohesion();
	PxVec3 FlowFieldFollow();
};