class Node
{
public:
	Node(XMFLOAT3 worldPosition, bool walkable, int gridX, int gridZ);
	~Node();

	bool operator>(Node other);

	//General
	const XMFLOAT3& GetPosition() const { return m_WorldPosition; }
	const bool& IsWalkable() const { return m_Walkable; }
	const int& GetX() const { return m_GridX; }
	const int& GetZ() const { return m_GridZ; }
	void SetOccupyingObject(GameObject* pObject) { m_pOccupyingObject = pObject; }
	void SetWalkable(bool walkable) { m_Walkable = walkable; }
	GameObject* GetOccupyingObject() const { return m_pOccupyingObject; }

	//A*
	int GetFCost() const { return m_gCost + m_hCost; }
	int& GetHCost() { return m_hCost; }
	int& GetGCost() { return m_gCost; }
	Node* GetParent() const { return m_pParent; }
	int& HeapIndex() { return m_HeapIndex; }
	void SetParent(Node* pNode) { m_pParent = pNode; }
	
	//Flow field
	int& GetDistance() { return m_Distance; }
	PxVec3& GetVector() { return m_Vector; }

private:
	//General variables
	XMFLOAT3 m_WorldPosition;
	int m_GridX;
	int m_GridZ;
	bool m_Walkable;
	GameObject* m_pOccupyingObject = nullptr;

	//A* variables
	Node* m_pParent = nullptr;
	int m_gCost = 0;
	int m_hCost = 0;
	int m_HeapIndex = 0;

	//Flow field variables
	PxVec3 m_Vector;
	int m_Distance = -1;
};

class NodeHeap
{
public:
	NodeHeap(int maxHeapSize);

	void Add(Node* item);
	Node* RemoveFirst();
	bool Contains(Node* item);
	const int& Size() const;

private:

	void UpdateItem(Node* item);
	void SortDown(Node* item);
	void SortUp(Node* item);
	void Swap(Node* itemA, Node* itemB);
	std::vector<Node*> m_Items;
	int m_CurrentItemCount = 0;
};