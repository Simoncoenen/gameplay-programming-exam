#include <Helpers/Singleton.h>
class Grid;
class Node;

class Pathfinding : public Singleton<Pathfinding>
{
public:
	Pathfinding();
	~Pathfinding();

	void Initialize(Grid* pGrid);
	std::vector<Node*> FindPath(const XMFLOAT3& start, const XMFLOAT3& target);
	std::vector<Node*> RetracePath(Node* pStartNode, Node* pTargetNode) const;
	Node* NodeFromWorldPoints(const XMFLOAT3& worldPosition) const;
	Grid* GetGrid() const { return m_pGrid; }
private:
	int GetDistance(const Node* a, const Node* b) const;
	Grid* m_pGrid;
};

