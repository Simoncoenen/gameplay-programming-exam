#pragma once
#include <vector>
#include <queue>
class GameScene;
class Node;
class QuadPrefab;

class Grid
{
public:
	Grid(XMFLOAT3 center, float width, float depth, float nodeSize);
	~Grid();

	void CreateGrid(GameScene* pGameScene);

	Node* NodeFromWorldPoints(const XMFLOAT3& worldPosition);
	std::vector<Node*> GetNeighbours(Node* pNode);
	int GetSize() const { return m_GridSizeX * m_GridSizeZ; }

	void UpdateFlowField(XMFLOAT3 goal, bool localOptimaFix);

	//Debugging
	void Debug();
	void DebugHeatmap(const GameContext& gameContext);
	void DebugFlowField();

private:
	//Flow field generation
	void LocalOptimaFix(Node* pNode, queue<Node*>& queue);
	void CreateVectorField();
	void CreateHeatmap(Node* pNode, queue<Node*> queue);

	std::vector<Node*> GetFlowfieldNeihbours(Node* pNode);

	GameScene* m_pGameScene;
	GameObject* NodeCheck(const XMFLOAT3& position) const;
	XMFLOAT3 m_Center;
	float m_GridWidth;
	float m_GridDepth;
	int m_GridSizeX;
	int m_GridSizeZ;
	float m_NodeSize;
	float m_NodeRadius;
	std::vector<Node*> m_Grid;
};