#include "stdafx.h"
#include "Pathfinding.h"
#include "Grid.h"
#include <deque>
#include "Node.h"

Pathfinding::Pathfinding():
	m_pGrid(nullptr)
{}

Pathfinding::~Pathfinding()
{}

void Pathfinding::Initialize(Grid* pGrid)
{
	m_pGrid = pGrid;
}

vector<Node*> Pathfinding::FindPath(const XMFLOAT3& start, const XMFLOAT3& target)
{
	Node* pStartNode = m_pGrid->NodeFromWorldPoints(start);
	Node* pTargetNode = m_pGrid->NodeFromWorldPoints(target);

	NodeHeap openSet(m_pGrid->GetSize());
	deque<Node*> closedSet;
	openSet.Add(pStartNode);

	while(openSet.Size() != 0)
	{
		Node* pCurrentNode = openSet.RemoveFirst();
		closedSet.push_back(pCurrentNode);

		if(pCurrentNode == pTargetNode)
		{
			vector<Node*> path = RetracePath(pStartNode, pTargetNode);
			return path;
		}

		for(Node* n : m_pGrid->GetNeighbours(pCurrentNode))
		{
			if (pCurrentNode->IsWalkable() == false || find(closedSet.begin(), closedSet.end(), n) != closedSet.end())
				continue;

			int newMovementCostToNeightbour = pCurrentNode->GetGCost() + GetDistance(pCurrentNode, n);
			if(newMovementCostToNeightbour < n->GetGCost() || openSet.Contains(n) == false)
			{
				n->GetGCost() = newMovementCostToNeightbour;
				n->GetHCost() = GetDistance(n, pTargetNode);
				n->SetParent(pCurrentNode);

				if(openSet.Contains(n) == false)
					openSet.Add(n);
			}
		}
	}
	Logger::LogWarning(L"Pathfinding: Path not found.");
	vector<Node*> empty;
	return empty;
}

vector<Node*> Pathfinding::RetracePath(Node* pStartNode, Node* pTargetNode) const
{
	vector<Node*> path;
	Node* pCurrentNode = pTargetNode;
	while (pCurrentNode != pStartNode)
	{
		path.push_back(pCurrentNode);
		pCurrentNode = pCurrentNode->GetParent();
		if (pCurrentNode == nullptr)
			break;
	}
	reverse(path.begin(), path.end());
	return path;
}

Node* Pathfinding::NodeFromWorldPoints(const XMFLOAT3& worldPosition) const
{
	return m_pGrid->NodeFromWorldPoints(worldPosition);
}

int Pathfinding::GetDistance(const Node* a, const Node* b) const
{
	int dstX = abs(a->GetX() - b->GetX());
	int dstZ = abs(a->GetZ() - b->GetZ());

	if (dstX > dstZ)
		return 14 * dstZ + 10 * (dstX - dstZ);
	return 14 * dstX + 10 * (dstZ - dstX);
}
