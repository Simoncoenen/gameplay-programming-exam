#include "stdafx.h"
#include "Grid.h"
#include <Scenegraph/GameScene.h>
#include <Prefabs/QuadPrefab.h>
#include <Components/Components.h>
#include <Physx/PhysxProxy.h>
#include "Node.h"
#include <Exam/Helpers/Enumerations.h>
#include <Diagnostics/DebugRenderer.h>
#include <Diagnostics/GeometryDebugRenderer.h>
#include <Graphics/TextRenderer.h>
#include <Content/ContentManager.h>

Grid::Grid(XMFLOAT3 center, float width, float depth, float nodeSize) :
	m_pGameScene(nullptr),
	m_Center(center),
	m_GridWidth(width),
	m_GridDepth(depth),
	m_NodeSize(nodeSize)
{
	m_NodeRadius = m_NodeSize / 2.0f;
	m_GridSizeX = static_cast<int>(width / m_NodeSize);
	m_GridSizeZ = static_cast<int>(depth / m_NodeSize);
}

Grid::~Grid()
{
	for(Node* n : m_Grid)
		if (n != nullptr)
			delete n;
}

void Grid::CreateGrid(GameScene* pGameScene)
{
	Timer a;
	m_pGameScene = pGameScene;
	m_Grid.resize(m_GridSizeX * m_GridSizeZ);
	XMFLOAT3 worldBottomLeft(m_Center.x - m_GridWidth / 2.0f, m_Center.y, m_Center.z - m_GridDepth / 2.0f);

	for (int z = 0; z < m_GridSizeZ; z++)
	{
		for (int x = 0; x < m_GridSizeX; x++)
		{
			int idx = x + z * m_GridSizeX;
			XMFLOAT3 worldPos(worldBottomLeft.x + (x * m_NodeSize + m_NodeRadius), worldBottomLeft.y, worldBottomLeft.z + (z * m_NodeSize + m_NodeRadius));

			GameObject* pOccupyingObject = NodeCheck(worldPos);
			bool walkable = false;
			if (pOccupyingObject == nullptr)
				walkable = true;
			m_Grid[idx] = new Node(worldPos, walkable, x, z);
			m_Grid[idx]->SetOccupyingObject(pOccupyingObject);
		}
	}
}

Node* Grid::NodeFromWorldPoints(const XMFLOAT3& worldPosition)
{
	int x = static_cast<int>((m_GridWidth / 2.0f + worldPosition.x) / m_NodeSize);
	int z = static_cast<int>((m_GridDepth / 2.0f + worldPosition.z) / m_NodeSize);
	Clamp(x, m_GridSizeX - 1, 0);
	Clamp(z, m_GridSizeZ - 1, 0);
	int idx = x + z * m_GridSizeX;

	return m_Grid[idx];
}

std::vector<Node*> Grid::GetNeighbours(Node* pNode)
{
	vector<Node*> neighbours;

	for (int x = -1; x <= 1; x++)
	{
		for (int z = -1; z <= 1; z++)
		{
			if (x == 0 && z == 0)
				continue;

			int currentX = pNode->GetX();
			int currentZ = pNode->GetZ();

			int checkX = currentX + x;
			int checkZ = currentZ + z;
			if (checkX >= 0 && checkX < m_GridSizeX && checkZ >= 0 && checkZ < m_GridSizeZ)
			{
				Node* nNode = m_Grid[checkX + checkZ * m_GridSizeX];
				if (x != 0 && z != 0)
				{
					int cornerCheckX = currentX + x + currentZ * m_GridSizeX;
					int cornerCheckZ = currentX + (currentZ + z) * m_GridSizeX;
					Clamp(cornerCheckX, static_cast<int>(m_Grid.size()) - 1, 0);
					Clamp(cornerCheckZ, static_cast<int>(m_Grid.size()) - 1, 0);

					if (m_Grid[cornerCheckX]->IsWalkable() == false || m_Grid[cornerCheckZ]->IsWalkable() == false)
						continue;
				}
				neighbours.push_back(nNode);
			}
		}
	}
	return neighbours;
}

void Grid::LocalOptimaFix(Node* pNode, queue<Node*>& queue)
{
	//Mark 3 neighbouring nodes extra

	int x;
	int z;
	if (pNode->GetX() == m_GridSizeX - 1)
		x = pNode->GetX() - 1;
	else
		x = pNode->GetX() + 1;
	if (pNode->GetZ() == m_GridSizeZ - 1)
		z = pNode->GetZ() - 1;
	else
		z = pNode->GetZ() + 1;

	int horizontalIdx = x + (pNode->GetZ()) * m_GridSizeX;
	int verticalIdx = pNode->GetX() + z * m_GridSizeX;
	int diagonalIdx = x + z * m_GridSizeX;
	if (m_Grid[horizontalIdx]->IsWalkable())
	{
		m_Grid[horizontalIdx]->GetDistance() = 0;
		queue.push(m_Grid[horizontalIdx]);
	}
	if (m_Grid[verticalIdx]->IsWalkable())
	{
		m_Grid[verticalIdx]->GetDistance() = 0;
		queue.push(m_Grid[verticalIdx]);
	}
	if (m_Grid[diagonalIdx]->IsWalkable())
	{
		m_Grid[diagonalIdx]->GetDistance() = 0;
		queue.push(m_Grid[diagonalIdx]);
	}
}

void Grid::UpdateFlowField(XMFLOAT3 goal, bool localOptimaFix)
{
	//Reset all the nodes
	for(Node* pNode : m_Grid)
		pNode->GetDistance() = -1;

	std::queue<Node*> queue;
	Node* pTargetNode = NodeFromWorldPoints(goal);
	queue.push(pTargetNode);

	if(localOptimaFix)
		LocalOptimaFix(pTargetNode, queue);

	pTargetNode->GetDistance() = 0;
	CreateHeatmap(pTargetNode, queue);
	CreateVectorField();
}

void Grid::CreateHeatmap(Node* pNode, queue<Node*> queue)
{
	std::vector<Node*> neighbours = GetFlowfieldNeihbours(pNode);
	for(Node* n : neighbours)
	{
		n->GetDistance() = pNode->GetDistance() + 1;
		queue.push(n);
	}
	if (queue.size() == 0)
		return;
	Node* next = queue.front();
	queue.pop();
	CreateHeatmap(next, queue);
}

void Grid::CreateVectorField()
{
	for (int z = 0; z < m_GridSizeZ; z++)
	{
		for (int x = 0; x < m_GridSizeX; x++)
		{
			int i = x + z * m_GridSizeX;
			if (m_Grid[i]->IsWalkable() == false)
				continue;

			PxVec3 vector;
			int left, right, up, down;
			left = right = up = down = m_Grid[i]->GetDistance();

			if (x - 1 >= 0 && m_Grid[i - 1]->IsWalkable())
				left = m_Grid[i - 1]->GetDistance();
			if (x + 1 < m_GridSizeX && m_Grid[i + 1]->IsWalkable())
				right = m_Grid[i + 1]->GetDistance();
			if (z - 1 >= 0 && m_Grid[i - m_GridSizeX]->IsWalkable())
				down = m_Grid[i - m_GridSizeX]->GetDistance();
			if (z + 1 < m_GridSizeZ && m_Grid[i + m_GridSizeX]->IsWalkable())
				up = m_Grid[i + m_GridSizeX]->GetDistance();
			
			vector.x = (float)left - right;
			vector.z = (float)down - up;


			/////
			if (vector.x < 0 && left == m_Grid[i]->GetDistance())
				vector.x = 0.0f;
			else if (vector.x > 0 && right == m_Grid[i]->GetDistance())
				vector.x = 0.0f;
			if (vector.z < 0 && down == m_Grid[i]->GetDistance())
				vector.z = 0.0f;
			else if (vector.z > 0 && up == m_Grid[i]->GetDistance())
				vector.z = 0.0f;

			m_Grid[i]->GetVector() = vector.getNormalized();
		}
	}
}

std::vector<Node*> Grid::GetFlowfieldNeihbours(Node* pNode)
{
	std::vector<Node*> neighbours;
	int x = pNode->GetX();
	int z = pNode->GetZ();
	int i = x + z * m_GridSizeX;

	if (x - 1 >= 0 && m_Grid[i - 1]->IsWalkable() && m_Grid[i - 1]->GetDistance() == -1)
		neighbours.push_back(m_Grid[i - 1]);
	if (x + 1 < m_GridSizeX && m_Grid[i + 1]->IsWalkable() && m_Grid[i + 1]->GetDistance() == -1)
		neighbours.push_back(m_Grid[i + 1]);
	if (z - 1 >= 0 && m_Grid[i - m_GridSizeX]->IsWalkable() && m_Grid[i - m_GridSizeX]->GetDistance() == -1)
		neighbours.push_back(m_Grid[i - m_GridSizeX]);
	if (z + 1 < m_GridSizeZ && m_Grid[i + m_GridSizeX]->IsWalkable() && m_Grid[i + m_GridSizeX]->GetDistance() == -1)
		neighbours.push_back(m_Grid[i + m_GridSizeX]);
	return neighbours;
}

GameObject* Grid::NodeCheck(const XMFLOAT3& position) const
{
	PhysxProxy *pProxy = m_pGameScene->GetPhysxProxy();
	PxSweepBuffer hit;
	PxQueryFilterData filterData;
	filterData.data.word0 = CollisionGroup::cStructure | CollisionGroup::cDestructible;
	if (pProxy->Sweep(PxSphereGeometry(m_NodeRadius - 1.0f), PxTransform(ToPxVec3(position) + PxVec3(0.0f, 8.0f, 0.0f)), PxVec3(1.0f, 0.0f, 0.0f), 0.0f, hit, PxHitFlag::eDEFAULT ,filterData))
	{
		auto actor = hit.block.actor;
		if (actor != nullptr)
		{
			auto userData = actor->userData;
			if (userData != nullptr)
			{
				return reinterpret_cast<BaseComponent*>(userData)->GetGameObject();
			}
		}
	}
	return nullptr;
}

//Debugging
void Grid::Debug()
{
	for (size_t i = 0; i < m_Grid.size(); i++)
	{
		XMFLOAT4 color(0.0f, 1.0f, 0.0f, 0.2f);
		bool walkable = m_Grid[i]->IsWalkable();
		if (!walkable)
			color = XMFLOAT4(1.0f, 0.0f, 0.0f, 0.2f);

		XMFLOAT3 pos = m_Grid[i]->GetPosition();
		pos.y += 0.1f;
		GeometryDebugRenderer::DrawQuad(pos, m_NodeSize - 0.1f, color);
	}
}

void Grid::DebugHeatmap(const GameContext& gameContext)
{
	int maxDistance = 0;
	for (Node* pNode : m_Grid)
	{
		if (pNode->GetDistance() > maxDistance)
			maxDistance = pNode->GetDistance();
	}

	for (size_t i = 0; i < m_Grid.size(); i++)
	{
		float clr = 1.0f - (float)m_Grid[i]->GetDistance() / maxDistance * 2;
		XMFLOAT4 color(0.0f, 0.0f, clr, 1.0f);
		bool walkable = m_Grid[i]->IsWalkable();
		if (!walkable)
			color = XMFLOAT4(1.0f, 0.0f, 0.0f, 0.7f);

		XMFLOAT3 pos = m_Grid[i]->GetPosition();
		pos.y += 0.1f;
		GeometryDebugRenderer::DrawQuad(pos, m_NodeSize - 0.1f, color);
	}

	SpriteFont* pSpriteFont = ContentManager::Load<SpriteFont>(L"Resources/Fonts/Calibri_20.fnt");
	for (Node* pNode : m_Grid)
	{
		XMFLOAT2 screenPos = gameContext.pCamera->WorldToScreenPoint(pNode->GetPosition());
		TextRenderer::GetInstance()->DrawText(pSpriteFont, to_wstring(pNode->GetDistance()), screenPos);
	}
}

void Grid::DebugFlowField()
{
	for (Node* pNode : m_Grid)
	{
		XMFLOAT3 lineStart = pNode->GetPosition();
		lineStart.y += 0.2f;
		XMFLOAT3 lineEnd;
		lineEnd.x = lineStart.x + pNode->GetVector().x * m_NodeRadius;
		lineEnd.y = lineStart.y;
		lineEnd.z = lineStart.z + pNode->GetVector().z * m_NodeRadius;
		DebugRenderer::DrawLine(lineStart, lineEnd, (XMFLOAT4)Colors::White);
	}
}