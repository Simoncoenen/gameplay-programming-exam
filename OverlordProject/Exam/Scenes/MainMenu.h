#pragma once
#include "Scenegraph/GameScene.h"
#pragma region
class Button;
class ButtonManager;
class TextureData;
class SpriteFont;
class MenuSelector;
class WindowSettings;
class HighscoreEntry;
#pragma endregion Class Forwards

class MainMenu : public GameScene
{
public:
	MainMenu(void);
	virtual ~MainMenu(void);

protected:

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);
	virtual void Draw(const GameContext& gameContext);

private:

	enum class CurrentWindow
	{
		MAIN,
		HIGHSCORES
	};
	CurrentWindow m_CurrentWindow = CurrentWindow::MAIN;

	void CreateButtons();
	void InitializeInput(const GameContext &gameContext);
	void StartGame();

	enum InputKey
	{
		LEFT,
		RIGHT,
		EXIT,
		START
	};

	//Buttons
	ButtonManager* m_pMainButtonManager = nullptr;
	ButtonManager* m_pLeaderboardsButtonManager = nullptr;
	MenuSelector* m_pLevelSelector = nullptr;

	TextureData* m_pStartButtonTexture = nullptr;

	TextureData* m_pMenuBackground = nullptr;

	//Fonts
	SpriteFont* m_pSmallFont = nullptr;
	SpriteFont* m_pTinyFont = nullptr;

	//Window info
	XMFLOAT2 m_ScreenMiddle;
	GameSettings::WindowSettings* m_WindowSettings = nullptr;

	//Leaderboards
	vector<HighscoreEntry> m_HighscoreEntries;

	//Sounds
	FMOD::Channel* m_pBackgroundChannel = nullptr;
	FMOD::Sound* m_pBackgroundMusic = nullptr;
	FMOD::Sound* m_pButtonClickSound = nullptr;
	FMOD::Sound* m_pButtonHoverSound = nullptr;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	MainMenu(const MainMenu &obj);
	MainMenu& operator=(const MainMenu& obj);
};

