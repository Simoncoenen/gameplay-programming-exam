class Button;
class TextureData;
class SpriteFont;

class MenuSelector
{
public:
	MenuSelector(const XMFLOAT2& position, TextureData* pCycleButtonDefault, TextureData* pCycleButtonHover, TextureData* pDataTexture, SpriteFont* pSpriteFont, FMOD::Sound* pHoverSound = nullptr, FMOD::Sound* pClickSound = nullptr);
	~MenuSelector();

	void Draw();
	void Update(const GameContext& gameContext);
	void AddData(const wstring& data) { m_Data.push_back(data); }
	void Next();
	void Previous();

	const wstring& GetSelected() { return m_Data[m_Selected]; }

private:
	Button* m_pLeftButton = nullptr;
	Button* m_pRightButton = nullptr;
	
	vector<wstring> m_Data;
	int m_Selected = 0;

	XMFLOAT2 m_Position;

	TextureData* m_pCycleButtonDefault;
	TextureData* m_pCycleButtonHover;
	TextureData* m_pDataTexture;

	SpriteFont* m_pSpriteFont;
};

