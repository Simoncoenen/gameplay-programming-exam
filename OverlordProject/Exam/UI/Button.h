class TextureData;
class SpriteFont;

class Button
{
public:
	Button(const XMFLOAT2& pos, function<void(void)> callBack, TextureData* pDefaultTexture, TextureData* pHoverTexture, FMOD::Sound* pHoverSound = nullptr, FMOD::Sound* pClickSound = nullptr);
	~Button() {}

	void Update(const GameContext& gameContext);
	void Draw();

private:

	function<void(void)> m_Callback;
	bool IsOnButton(const POINT &point) const;

	bool m_IsOnButton = false;
	bool m_IsPressed = false;

	XMFLOAT2 m_Position;
	XMFLOAT4 m_Color;

	TextureData* m_pDefaultTexture, *m_pHoverTexture;

	//Sounds
	FMOD::Sound* m_pHoverSound = nullptr;
	FMOD::Sound* m_pClickSound = nullptr;
};