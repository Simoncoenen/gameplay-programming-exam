#include "stdafx.h"
#include "MenuSelector.h"
#include "Button.h"

#include <Graphics/TextRenderer.h>
#include <Graphics/SpriteRenderer.h>
#include <Graphics/TextureData.h>


MenuSelector::MenuSelector(const XMFLOAT2& position, TextureData* pCycleButtonDefault, TextureData* pCycleButtonHover, TextureData* pDataTexture, SpriteFont* pSpriteFont, FMOD::Sound* pHoverSound, FMOD::Sound* pClickSound):
	m_Position(position),
	m_pCycleButtonDefault(pCycleButtonDefault),
	m_pCycleButtonHover(pCycleButtonHover),
	m_pDataTexture(pDataTexture),
	m_pSpriteFont(pSpriteFont)
{
	m_pLeftButton = new Button(XMFLOAT2(position.x - 200.0f, position.y), [this]() {Previous(); }, pCycleButtonDefault, pCycleButtonHover, pHoverSound, pClickSound);
	m_pRightButton = new Button(XMFLOAT2(position.x + 200.0f, position.y), [this]() {Next(); }, pCycleButtonDefault, pCycleButtonHover, pHoverSound, pClickSound);
}

MenuSelector::~MenuSelector()
{
	SafeDelete(m_pLeftButton);
	SafeDelete(m_pRightButton);
}

void MenuSelector::Draw()
{
	m_pLeftButton->Draw();
	m_pRightButton->Draw();

	float xPos = m_Position.x - m_Data[m_Selected].size() * 13.0f;
	TextRenderer::GetInstance()->DrawText(m_pSpriteFont, m_Data[m_Selected], XMFLOAT2(xPos, m_Position.y - 25.0f), XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f));
	TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"<", XMFLOAT2(m_Position.x - 210.0f, m_Position.y - 25.0f), XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f));
	TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L">", XMFLOAT2(m_Position.x + 190.0f, m_Position.y - 25.0f), XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f));
	XMFLOAT2 pos;
	pos.x = m_Position.x - m_pDataTexture->GetDimension().x / 2.0f;
	pos.y = m_Position.y - m_pDataTexture->GetDimension().y / 2.0f;
	SpriteRenderer::GetInstance()->Draw(m_pDataTexture, pos);
}

void MenuSelector::Update(const GameContext& gameContext)
{
	m_pLeftButton->Update(gameContext);
	m_pRightButton->Update(gameContext);
}

void MenuSelector::Next()
{
	m_Selected++; if (m_Selected >= (int)m_Data.size()) m_Selected = 0;
}

void MenuSelector::Previous()
{
	m_Selected--; if (m_Selected < 0) m_Selected = (int)m_Data.size() - 1;
}