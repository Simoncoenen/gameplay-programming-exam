#include "stdafx.h"
#include "OfflineLeaderboard.h"
#include "HighscoreEntry.h"
#include <ctime>

OfflineLeaderboard::OfflineLeaderboard()
{}

OfflineLeaderboard::~OfflineLeaderboard()
{}

bool OfflineLeaderboard::RegisterHighscore(int score, float playTime)
{
	vector<HighscoreEntry> entries = GetEntries();

	//Get the username of the current user
	wchar_t username[50 + 1];
	DWORD username_len = 50 + 1;
	GetUserName(username, &username_len);
	wstring name = wstring(username, username + username_len - 1);

	return RegisterHighscore(name, score, playTime);
}

bool OfflineLeaderboard::RegisterHighscore(const wstring& name, int score, float playTime)
{
	//Format the name
	wstring newName = FormatName(name);

	vector<HighscoreEntry> entries = GetEntries();

	//Get the date
	wstringstream date;
	time_t timer;
	time(&timer);
	tm localTime;
	localtime_s(&localTime, &timer);
	date << localTime.tm_mday << "-" << localTime.tm_mon + 1 << "-" << 1900 + localTime.tm_year;

	//Get the time
	wstringstream time;
	time << localTime.tm_hour << ":" << localTime.tm_min << ":" << localTime.tm_sec;
	//Add the new entry
	HighscoreEntry newEntry;
	newEntry.Name = newName;
	newEntry.Score = score;
	newEntry.Date = date.str();
	newEntry.Time = time.str();
	newEntry.PlayTime = static_cast<int>(trunc(playTime));
	entries.push_back(newEntry);

	//Sort the entries by score
	sort(entries.begin(), entries.end(), [](const HighscoreEntry& a, const HighscoreEntry& b)
	{
		return a.Score > b.Score;
	});

	//Write the new scores to file
	wofstream oFile("./Resources/Levels/Highscores.txt");

	for (size_t i = 0; i < entries.size(); i++)
	{
		if (i > 10)
			break;
		//Example format: simon|516351|20/05/16|21:13:15|5135
		oFile << entries[i].Name << "|" << entries[i].Score << "|" << entries[i].Date << endl << "|" << entries[i].Time << "|" << entries[i].PlayTime;
	}
	oFile.close();
	return true;
}

vector<HighscoreEntry> OfflineLeaderboard::GetEntries(int amount)
{
	//Read the whole file
	wifstream iFile(L"./Resources/Levels/Highscores.txt");
	if (iFile.fail())
	{
		Logger::LogWarning(L"OfflineLeaderboard::GetEntries > File not found!");
		vector<HighscoreEntry> empty;
		return empty;
	}

	//Save all the entries
	wstring line;
	vector<HighscoreEntry> entries;
	while (getline(iFile, line) && static_cast<int>(entries.size()) < amount)
	{
		entries.push_back(ParseHighscoreLine(line));
	}
	iFile.close();

	return entries;
}

HighscoreEntry OfflineLeaderboard::ParseHighscoreLine(const wstring& line) const
{
	HighscoreEntry entry;

	int startIdx = 0;
	int endIdx = line.find('|', startIdx);
	entry.Name = line.substr(startIdx, endIdx - startIdx);

	startIdx = endIdx + 1;
	endIdx = line.find('|', startIdx);
	entry.Score = stoi(line.substr(startIdx, endIdx - startIdx));

	startIdx = endIdx + 1;
	endIdx = line.find('|', startIdx);
	entry.Date = line.substr(startIdx, endIdx - startIdx);

	startIdx = endIdx + 1;
	endIdx = line.find('|', startIdx);
	entry.Time = line.substr(startIdx, endIdx - startIdx);

	startIdx = endIdx + 1;
	endIdx = line.find('|', startIdx);
	entry.PlayTime = stoi(line.substr(startIdx, endIdx - startIdx));

	return entry;
}

wstring OfflineLeaderboard::FormatName(const wstring& name) const
{
	if (name.size() < 2)
	{
		Logger::LogWarning(L"OfflineLeaderboard::FormatName > Name too short!");
		return name;
	}
	wstringstream nameStream;
	for (size_t i = 0; i < name.size(); i++)
	{
		if (name[i] == ' ')
			continue;
		if (i == 0)
			nameStream << static_cast<char>(toupper(name[i]));
		else
			nameStream << name[i];
	}
	return nameStream.str();
}
