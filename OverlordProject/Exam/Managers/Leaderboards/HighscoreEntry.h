#pragma once
class HighscoreEntry
{
public:
	HighscoreEntry(){}
	~HighscoreEntry(){}

	wstring Name;
	int Score = 0;
	wstring Date;
	wstring Time;
	int PlayTime = 0;
};