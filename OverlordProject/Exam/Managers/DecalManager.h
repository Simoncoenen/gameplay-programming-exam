#include <Helpers/Singleton.h>
#include <deque>

class DecalManager : public Singleton<DecalManager>
{
public:
	DecalManager();
	~DecalManager();

	enum Type : int
	{
		BLOOD,
		EXPLOSION
	};

	void Add(const XMFLOAT3& position, const Type type, GameScene* pScene);
	GameObject* CreateDecal(const Type type, const XMFLOAT3& position);
private:
	deque<GameObject*> m_Decals;
	int m_MaxSize;
	int m_HeightCount;
	float m_BeginHeight;
};