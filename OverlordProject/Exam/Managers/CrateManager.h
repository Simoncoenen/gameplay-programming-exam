#include <Scenegraph\GameObject.h>

class Crate;

//MANAGER
class CrateManager : public GameObject
{
public:
	CrateManager();
	~CrateManager();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void SetSpawnpoints(vector<XMFLOAT3> spawnpoints) { m_Spawnpoints = spawnpoints; }
	void SpawnAllCrates();
private:
	void RemoveCrate(GameObject* pCrate);
	vector<Crate*> m_Crates;
	vector<XMFLOAT3> m_Spawnpoints;
	float m_MinimumRespawnTime = 10.0f;
	float m_MaximumRespawnTime = 30.0f;
	float m_RespawnTimer = 0.0f;
	float m_RespawnTime = 0.0f;
};