#include "../helpers/json.hpp"
class GameScene;
class CrateManager;
class EnemyManager;
class MeshFilter;

class LevelLoader
{
public:
	LevelLoader();
	~LevelLoader();
	void Initialize(GameScene* pGameScene);

	void LoadLevel(const wstring &filePath);
	std::vector<XMFLOAT3> GetCrateSpawnpoints() const { return m_CrateSpawnpoints; }
	std::vector<XMFLOAT3> GetEnemySpawnpoints() const { return m_EnemySpawnpoints; }
	const XMFLOAT3 &GetPlayableArea() const { return m_PlayableArea; }
	const XMFLOAT3 &GetPlayerStart() const { return m_PlayerStart; }
private:

	void CreateStructures(nlohmann::json& levelData);
	void CreateGround(nlohmann::json& levelData);
	void CreateObjects(nlohmann::json& levelData) const;
	void SetCrateSpawns(nlohmann::json& levelData);
	void SetEnemySpawns(nlohmann::json& levelData);
	void SetPlayerStart(nlohmann::json& levelData);

	std::vector<XMFLOAT3> m_CrateSpawnpoints;
	std::vector<XMFLOAT3> m_EnemySpawnpoints;
	XMFLOAT3 m_PlayableArea;
	XMFLOAT3 m_PlayerStart;
	GameScene* m_pGameScene;
	PxMaterial* m_pDefaultMaterial;

	//Batch for structures
	MeshFilter* m_pMasterMeshFilter = nullptr;
	GameObject* m_pMasterObject = nullptr;
};