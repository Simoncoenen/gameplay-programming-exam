#include "Weapon.h"

class FakeWallWeapon :public Weapon
{
public:
	FakeWallWeapon();
	~FakeWallWeapon();

	void Initialize(const GameContext& gameContext);
	void Shoot(const GameContext& gameContext);
};