#include "Weapon.h"

class UziWeapon final : public Weapon
{
public:
	UziWeapon();
	~UziWeapon();

	void Initialize(const GameContext& gameContext);
	void Shoot(const GameContext& gameContext);
};