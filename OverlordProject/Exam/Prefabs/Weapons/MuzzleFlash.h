#pragma once
#include <Scenegraph\GameObject.h>

class ModelComponent;

class MuzzleFlash :	public GameObject
{
public:
	MuzzleFlash();
	~MuzzleFlash();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void Activate();
private:
	float m_VisibilityTime = 0.1f;
	float m_Timer = m_VisibilityTime;
	ModelComponent* m_pModelComponent = nullptr;
};

