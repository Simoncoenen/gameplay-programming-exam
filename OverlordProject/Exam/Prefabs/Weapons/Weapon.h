#pragma once
#include "./Scenegraph/GameObject.h"

class ModelComponent;
class MuzzleFlash;

class Weapon : public GameObject
{
public:
	Weapon();
	virtual ~Weapon();

	//Delete copy- and assignment constructor
	Weapon& operator=(const Weapon& other) = delete;
	Weapon(const Weapon &other) = delete;

	virtual void Initialize(const GameContext& gameContext) = 0;
	virtual void Shoot(const GameContext& gameContext) = 0;

	void Update(const GameContext& gameContext);

	//Setters/Getters
	void SetAmmo(const int ammo, const bool overrideMaxAmmo = false);
	const int& GetAmmo() const { return m_Ammo; }
	void AddAmmo(const int ammo);
	void SetFireRate(const float fireRate);
	void SetActive(bool active);
	void SetBulletSpawnpoint(GameObject* pSpawn) { m_pSpawnSocket = pSpawn; }
	void SetRange(const float range) { m_Range = range; }
	void SetDamage(const int damage) { m_Damage = damage; }
	const int& GetDamage() const { return m_Damage; }
	bool IsMaxed() const { return m_Ammo >= m_MaxAmmo; }

protected:

	//Methods
	GameObject* GetHit(const PxVec3& origin, const PxVec3& unitDir, const PxReal distance);
	void DealDamage(GameObject* pObject);

	//Variables
	bool m_Active = true;
	int m_Ammo = 10;
	int m_MaxAmmo = 10;
	int m_Damage = 1;
	float m_FireRate = 0.2f;
	float m_FireTimer = 0.0f;
	float m_Range = 100.0f;

	MuzzleFlash* m_pMuzzleFlash = nullptr;
	GameObject* m_pSpawnSocket = nullptr;
	ModelComponent* m_pModelComponent = nullptr;

	//Sounds
	FMOD::Channel* m_pChannel = nullptr;
	FMOD::Sound* m_pSound = nullptr;
};

