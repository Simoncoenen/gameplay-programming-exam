#include "Weapon.h"

class MineWeapon :public Weapon
{
public:
	MineWeapon();
	~MineWeapon();

	void Initialize(const GameContext& gameContext);
	void Shoot(const GameContext& gameContext);
};