#include "stdafx.h"
#include "DefaultPlayer.h"
#include <Exam/Helpers/Enumerations.h>
#include <Exam/AI/Pathfinding/Pathfinding.h>
#include <Graphics/ModelAnimator.h>
#include <Components/Components.h>
#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/Prefabs/Weapons/Weapon.h>

DefaultPlayer::DefaultPlayer()
{
}


DefaultPlayer::~DefaultPlayer()
{
}

void DefaultPlayer::Update(const GameContext& gameContext)
{
	if (m_Dead)
		return;
	Player::Update(gameContext);
	Move(gameContext);
	ShootBehavior(gameContext);
}

void DefaultPlayer::ShootBehavior(const GameContext& gameContext)
{
	float triggerPressure = gameContext.pInput->GetTriggerPressure(false);
	if (gameContext.pInput->IsActionTriggered(PlayerInput::SHOOT) || triggerPressure > 0.2f)
	{
		//If current weapon doesn't have ammo, skip to the next one and return
		if (m_WeaponsArr[m_CurrentWeapon]->GetAmmo() <= 0 && m_UnlockedWeapons > 0)
			NextWeapon();
		else
			m_WeaponsArr[m_CurrentWeapon]->Shoot(gameContext);
	}
}

void DefaultPlayer::Move(const GameContext& gameContext)
{
	//Accumulate inputs and calculate resulting input to apply to the controller

	XMFLOAT3 playerPos = GetTransform()->GetWorldPosition();

	float deltaTime = gameContext.pGameTime->GetElapsed();
	PxVec3 moveDirection = PxVec3(0.0f, 0.0f, 0.0f);
	if (gameContext.pInput->IsActionTriggered(PlayerInput::FORWARD) && playerPos.z < m_Bounds.z / 2)
		moveDirection += PxVec3(0.0f, 0.0f, 1.0f);
	if (gameContext.pInput->IsActionTriggered(PlayerInput::BACKWARD) && playerPos.z > -m_Bounds.z / 2)
		moveDirection -= PxVec3(0.0f, 0.0f, 1.0f);
	if (gameContext.pInput->IsActionTriggered(PlayerInput::RIGHT) && playerPos.x < m_Bounds.x / 2)
		moveDirection += PxVec3(1.0f, 0.0f, 0.0f);
	if (gameContext.pInput->IsActionTriggered(PlayerInput::LEFT) && playerPos.x > -m_Bounds.x / 2)
		moveDirection -= PxVec3(1.0f, 0.0f, 0.0f);

	//Controller movement
	XMFLOAT2 controllerInput = gameContext.pInput->GetThumbstickPosition();
	moveDirection.x += controllerInput.x;
	moveDirection.z += controllerInput.y;
	moveDirection.x = static_cast<int>(moveDirection.x * 2) / 2.0f;
	moveDirection.z = static_cast<int>(moveDirection.z * 2) / 2.0f;
	moveDirection.normalize();

	moveDirection.x *= m_MoveSpeed * deltaTime;
	moveDirection.y *= m_MoveSpeed * deltaTime;
	moveDirection.z *= m_MoveSpeed * deltaTime;

	XMFLOAT3 up = { 0.0f, 1.0f, 0.0f };

	//Rotate towards the movement direction

	if (moveDirection.magnitude() != 0)
	{
		m_LastDirection = ToXMFLOAT3(moveDirection);
		m_pAnimator->Play();
	}
	else
		m_pAnimator->Pause();
	XMVECTOR rotation = CreateLookRotation(XMLoadFloat3(&m_LastDirection), XMLoadFloat3(&up));
	GetTransform()->Rotate(rotation);

	//Apply gravity
	moveDirection.y += m_Gravity * deltaTime;
	//Apply the movement to the controller
	m_pController->Move(moveDirection);
}