#pragma once
#include "Player.h"
class PointAndClickPlayer :
	public Player
{
public:
	PointAndClickPlayer();
	~PointAndClickPlayer();

	void Update(const GameContext& gameContext);

private:
	bool TargetInSight(const GameContext& gameContext);
	void ClickTarget(const GameContext& gameContext);

	void DebugPath();

	void PointAndClickMove(const GameContext& gameContext);
	void PointAndClickShoot(const GameContext& gameContext);

	//Path following
	PxVec3 PathFollow();
	vector<Node*> m_Path;
	int m_CurrentPathIndex = 0;
	float m_NodeRadius = 2.0f;

	PxVec3 MoveToTarget();
	float m_ArrivalDistance = 10.0f;

	XMFLOAT4 m_ClickLookDirection;
	PxVec3 m_MoveDirection = { 0.0f, 0.0f, 0.0f };
	PxVec3 m_LastVelocity = { 1.0f, 0.0f, 0.0f };
	XMFLOAT3 m_ClickPosition = {0.0f, 0.0f, 0.0f};
	bool m_TargetInSight = false;
	bool m_ClickInitialized = false;

	float m_Avg = 0;
	int m_Amount = 0;
};

