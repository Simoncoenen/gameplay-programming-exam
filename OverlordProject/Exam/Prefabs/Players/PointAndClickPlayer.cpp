#include "stdafx.h"
#include "PointAndClickPlayer.h"
#include <Exam/Helpers/Enumerations.h>
#include <Exam/AI/Pathfinding/Pathfinding.h>
#include <Graphics/ModelAnimator.h>
#include <Components/Components.h>
#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/Prefabs/Weapons/Weapon.h>
#include <Scenegraph/GameScene.h>
#include <Physx/PhysxProxy.h>
#include <Diagnostics/DebugRenderer.h>

PointAndClickPlayer::PointAndClickPlayer()
{
}


PointAndClickPlayer::~PointAndClickPlayer()
{
}

void PointAndClickPlayer::Update(const GameContext& gameContext)
{
	if (m_Dead)
		return;
	Player::Update(gameContext);

	ClickTarget(gameContext);
	PointAndClickMove(gameContext);
	PointAndClickShoot(gameContext);
}

bool PointAndClickPlayer::TargetInSight(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	PhysxProxy* proxy = GetScene()->GetPhysxProxy();
	PxVec3 origin = GetTransform()->GetWorldPositionPcVec3();
	PxVec3 target = ToPxVec3(m_ClickPosition);
	target.y = origin.y = 1.0f;
	PxVec3 direction = target - origin;
	direction.normalize();
	PxRaycastBuffer hit;
	PxQueryFilterData filterData = PxQueryFilterData();
	filterData.data.word0 = CollisionGroup::cStructure | CollisionGroup::cDestructible;
	DebugRenderer::DrawLine(ToXMFLOAT3(origin), ToXMFLOAT3(origin + direction * (target - origin).magnitude()));
	return !proxy->Raycast(origin, direction, (target - origin).magnitude(), hit, PxHitFlag::eDEFAULT, filterData);
}

void PointAndClickPlayer::ClickTarget(const GameContext& gameContext)
{
	if (gameContext.pInput->IsActionTriggered(RIGHTCLICK))
	{
		XMFLOAT3 playerPosition = GetTransform()->GetWorldPosition();
		POINT mp = gameContext.pInput->GetMousePosition();
		XMFLOAT2 mousePosition((float)mp.x, (float)mp.y);

		m_ClickPosition = gameContext.pCamera->ScreenToWorldPoint(mousePosition, CollisionGroup::cEnemy | CollisionGroup::cPlayer);
		Timer a;
		m_Path = Pathfinding::GetInstance()->FindPath(playerPosition, m_ClickPosition);
		m_CurrentPathIndex = 0;
		m_TargetInSight = false;
		m_ClickInitialized = true;
		m_MoveDirection = MoveToTarget() * m_MoveSpeed * gameContext.pGameTime->GetElapsed();
		m_pAnimator->Play();
	}
}

void PointAndClickPlayer::DebugPath()
{
	if (m_Path.size() < 2)
		return;
	for (size_t i = 0; i < m_Path.size() - 1; i++)
	{
		XMFLOAT3 a = m_Path[i]->GetPosition();
		XMFLOAT3 b = m_Path[i+1]->GetPosition();
		a.y = b.y = 1.0f;
		DebugRenderer::DrawLine(a, b, (XMFLOAT4)Colors::Orange);
	}
}

void PointAndClickPlayer::PointAndClickMove(const GameContext& gameContext)
{
	float animSpeed = m_pController->GetVelocity().magnitude() / m_MoveSpeed;
	m_pAnimator->SetAnimationSpeed(animSpeed);

	XMVECTOR rotation = XMLoadFloat4(&m_ClickLookDirection);
	GetTransform()->Rotate(rotation);

	PxVec3 moveDirection(0, 0, 0);
	if (m_ClickInitialized)
	{
		DebugPath();
		if (TargetInSight(gameContext))
			moveDirection += MoveToTarget();
		else
			moveDirection += PathFollow();
		moveDirection *= m_MoveSpeed;
	}
	//Apply gravity
	moveDirection.y += m_Gravity;
	moveDirection *= gameContext.pGameTime->GetElapsed();
	m_MoveDirection = MoveTowards(m_MoveDirection, moveDirection, gameContext.pGameTime->GetElapsed() * 3.0f);
	m_pController->Move(m_MoveDirection);

	if (m_pController->GetVelocity().magnitude() > 1.0f)
		m_LastVelocity = m_pController->GetVelocity();
}

void PointAndClickPlayer::PointAndClickShoot(const GameContext& gameContext)
{
	XMFLOAT3 pos = GetTransform()->GetWorldPosition();
	POINT p = gameContext.pInput->GetMousePosition();
	XMFLOAT2 mousePos((float)p.x, (float)p.y);
	XMFLOAT3 worldPos = gameContext.pCamera->ScreenToWorldPoint(mousePos, CollisionGroup::cEnemy);
	XMVECTOR playerPos = XMLoadFloat3(&pos);

	XMVECTOR lookTarget = XMLoadFloat3(&worldPos);
	XMVECTOR lookDir = lookTarget - playerPos;
	lookDir.m128_f32[1] = 0.0f;
	XMFLOAT3 up(0, 1, 0);
	XMVECTOR upDir = XMLoadFloat3(&up);
	XMStoreFloat4(&m_ClickLookDirection, CreateLookRotation(lookDir, upDir));
	if (gameContext.pInput->IsActionTriggered(LEFTCLICK))
		m_WeaponsArr[m_CurrentWeapon]->Shoot(gameContext);
}

PxVec3 PointAndClickPlayer::PathFollow()
{
	if (m_Path.size() == 0)
		return PxVec3(0, 0, 0);
	PxVec3 currPos = GetTransform()->GetWorldPositionPcVec3();
	PxVec3 targetPos = ToPxVec3(m_Path[m_CurrentPathIndex]->GetPosition());
	if(DistanceXZ(currPos, targetPos) <= m_NodeRadius)
	{
		if (m_CurrentPathIndex == (int)m_Path.size() - 1)
			return PxVec3(0, 0, 0);
		++m_CurrentPathIndex;
	}
	PxVec3 direction = targetPos - currPos;
	direction.y = 0;
	direction.normalize();
	return direction;
}

PxVec3 PointAndClickPlayer::MoveToTarget()
{
	PxVec3 currPos = GetTransform()->GetWorldPositionPcVec3();
	PxVec3 targetPos = ToPxVec3(m_ClickPosition);
	float distance = DistanceXZ(currPos, targetPos);
	PxVec3 desired = targetPos - currPos;
	desired.y = 0;
	desired.normalize();

	if (distance < 1.0f)
		return PxVec3(0, 0, 0);
	if (distance < m_ArrivalDistance)
		desired *= distance / m_ArrivalDistance;
	return desired;
}