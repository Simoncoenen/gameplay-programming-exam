#pragma once
#include "Player.h"
class DefaultPlayer :
	public Player
{
public:
	DefaultPlayer();
	~DefaultPlayer();

	void Update(const GameContext& gameContext);
private:
	void ShootBehavior(const GameContext& gameContext);
	void Move(const GameContext& gameContext);
};

