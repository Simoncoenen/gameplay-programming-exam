#pragma once
#include <Scenegraph\GameObject.h>

class MeshDrawComponent;
class TextureData;

class ClothObject :	public GameObject
{
public:
	ClothObject(float width, float height, float vertexDensity = 1.0f);
	~ClothObject();

	void Initialize(const GameContext& gameContext);
	void PostInitialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void Draw(const GameContext& gameContext);
	void SetTexture(TextureData* pTexture);

	void SetExternalAcceleration(const PxVec3& externalAcceleration) { m_ExternalAcceleration = externalAcceleration; }

	PxCloth* GetCloth() const { return m_pCloth; }
private:

	void CreateVertices();
	void CreateIndices();
	void CreateCloth();
	void LoadEffect(const GameContext& gameContext);
	void CreateBuffers(const GameContext& gameContext);

	vector<VertexPosNormTex> m_Vertices;
	vector<DWORD> m_Indices; 

	ID3D11Buffer* m_pVertexBuffer = nullptr;
	ID3D11Buffer* m_pIndexBuffer = nullptr;

	vector<PxClothParticle> m_ClothParticles;

	float m_Width;
	float m_Height;
	float m_VertexDensity;

	int m_VerticesX = 0;
	int m_VerticesY= 0;

	TextureData* m_pTexture = nullptr;

	//Components
	PxCloth* m_pCloth = nullptr;
	PxVec3 m_ExternalAcceleration;

	//Effect variables
	ID3DX11Effect* m_pEffect = nullptr;
	ID3DX11EffectTechnique* m_pTechnique = nullptr;
	ID3D11InputLayout* m_pInputLayout = nullptr;
	static ID3DX11EffectMatrixVariable *m_pWorldVar;
	static ID3DX11EffectMatrixVariable *m_pWvpVar;
	static ID3DX11EffectShaderResourceVariable *m_pTextureVariable;
};

