#include "stdafx.h"
#include "Explosion.h"

#include <Components/ParticleEmitterComponent.h>

Explosion::Explosion(const wstring assetPath, const float lifeTime) :
	m_LifeTime(lifeTime),
	m_AssetPath(assetPath)
{}

Explosion::~Explosion()
{}

void Explosion::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	ParticleEmitterComponent* pEmitter = new ParticleEmitterComponent(m_AssetPath);
	pEmitter->SetVelocity(XMFLOAT3(0, 6.0f, 0));
	pEmitter->SetMinSize(4.0f);
	pEmitter->SetMaxSize(6.0f);
	pEmitter->SetMinEnergy(5.0f);
	pEmitter->SetMaxEnergy(6.0f);
	pEmitter->SetMinSizeGrow(8.0f);
	pEmitter->SetMaxSizeGrow(20.0f);
	pEmitter->SetMinEmitterRange(1.0f);
	pEmitter->SetMaxEmitterRange(5.0f);
	pEmitter->SetColor(XMFLOAT4(1.f, 1.f, 1.f, 0.6f));
	AddComponent(pEmitter);
}

void Explosion::Update(const GameContext& gameContext)
{
	m_LifeTime -= gameContext.pGameTime->GetElapsed();
	if (m_LifeTime <= 0.0f)
		Destroy();
}