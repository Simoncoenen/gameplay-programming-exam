#pragma once
#include "Graphics/PostProcessingMaterial.h"

class ID3D11EffectShaderResourceVariable;

class PostGrayscale : public PostProcessingMaterial
{
public:
	PostGrayscale();
	~PostGrayscale();

	void SetStrength(const float strength);

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable* m_pTextureMapVariabele;
	static ID3DX11EffectScalarVariable* m_pStrengthVariable;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostGrayscale(const PostGrayscale &obj);
	PostGrayscale& operator=(const PostGrayscale& obj);
};
