//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"
#include "PostVignette.h"
#include "Graphics/RenderTarget.h"

ID3DX11EffectShaderResourceVariable*	PostVignette::m_pTextureMapVariable = nullptr;
ID3DX11EffectScalarVariable*			PostVignette::m_pRadiusVariable = nullptr;
ID3DX11EffectScalarVariable*			PostVignette::m_pSmoothnessVariable = nullptr;
ID3DX11EffectVectorVariable*			PostVignette::m_pColorVariable = nullptr;
ID3DX11EffectVectorVariable*			PostVignette::m_pCenterVariable = nullptr;

PostVignette::PostVignette()
	: PostProcessingMaterial(L"./Resources/Effects/Post/Vignette.fx")
{
}

PostVignette::~PostVignette(void)
{
}

void PostVignette::LoadEffectVariables()
{
	m_pTextureMapVariable = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
	if (!m_pTextureMapVariable->IsValid())
		Logger::LogWarning(L"PostVignette: GetVariableByName 'gTexture' not valid!");

	m_pColorVariable = m_pEffect->GetVariableByName("gColor")->AsVector();
	if (!m_pColorVariable->IsValid())
		Logger::LogWarning(L"PostVignette: GetVariableByName 'gColor' not valid!");

	m_pRadiusVariable = m_pEffect->GetVariableByName("gRadius")->AsScalar();
	if (!m_pRadiusVariable->IsValid())
		Logger::LogWarning(L"PostVignette: GetVariableByName 'gRadius' not valid!");

	m_pSmoothnessVariable = m_pEffect->GetVariableByName("gSmoothness")->AsScalar();
	if (!m_pSmoothnessVariable->IsValid())
		Logger::LogWarning(L"PostVignette: GetVariableByName 'gSmoothness' not valid!");

	m_pCenterVariable = m_pEffect->GetVariableByName("gCenter")->AsVector();
	if (!m_pSmoothnessVariable->IsValid())
		Logger::LogWarning(L"PostVignette: GetVariableByName 'gCenter' not valid!");
}

void PostVignette::UpdateEffectVariables(RenderTarget* rendertarget)
{
	//Update the TextureMapVariable with the Color ShaderResourceView of the given RenderTarget
	m_pTextureMapVariable->SetResource(rendertarget->GetShaderResourceView());
	m_pColorVariable->SetFloatVector(reinterpret_cast<float*>(&m_Color));
	m_pRadiusVariable->SetFloat(m_Radius);
	m_pSmoothnessVariable->SetFloat(m_Smoothness);
	m_pCenterVariable->SetFloatVector(reinterpret_cast<float*>(&m_Center));
}