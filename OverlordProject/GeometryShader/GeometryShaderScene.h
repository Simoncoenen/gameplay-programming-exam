#pragma once
#include "Scenegraph/GameScene.h"

class ModelComponent;
class SpriteFont;
class GeneratedTerrainComponent;
class ButtonManager;
class TextureData;

class GeometryShaderScene : public GameScene
{
public:
	GeometryShaderScene(void);
	virtual ~GeometryShaderScene(void);

protected:

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);
	virtual void Draw(const GameContext& gameContext);

private:

	ModelComponent* m_pModel = nullptr;
	GeneratedTerrainComponent* m_pTerrain = nullptr;
	ButtonManager* m_pButtonManager = nullptr;
	SpriteFont* m_pSpriteFont = nullptr;

	TextureData* m_pCycleButtonDefault = nullptr;
	TextureData* m_pCycleButtonHover = nullptr;
	TextureData* m_pButtonDefault = nullptr;
	TextureData* m_pButtonHover = nullptr;

	CameraComponent* m_pPrevCam = nullptr;

	float m_MidX = 0.0f;
	float m_MidY = 0.0f;

	bool m_RandomSeed = false;
	bool m_HalfLambert = true;
	bool m_Dirty = false;
	bool m_InterfaceEnabled = true;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	GeometryShaderScene(const GeometryShaderScene &obj);
	GeometryShaderScene& operator=(const GeometryShaderScene& obj);
};

