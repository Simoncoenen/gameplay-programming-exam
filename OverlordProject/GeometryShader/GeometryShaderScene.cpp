//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "GeometryShaderScene.h"
#include "Scenegraph\GameObject.h"
#include "Content\ContentManager.h"
#include "GeneratedTerrainComponent.h"

#include <Exam/UI/Button.h>
#include <Exam/UI/ButtonManager.h>
#include <Base/OverlordGame.h>
#include <Graphics/TextRenderer.h>
#include <Prefabs/FixedCamera.h>
#include <Components/Components.h>
#include <Graphics/SpriteRenderer.h>
#include <Materials/SkyboxMaterial.h>

GeometryShaderScene::GeometryShaderScene(void) :
	GameScene(L"HardwareSkinningScene"),
	m_pModel(nullptr)
{
}


GeometryShaderScene::~GeometryShaderScene(void)
{
	SafeDelete(m_pButtonManager);
}

void GeometryShaderScene::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//Camera
	m_pPrevCam = gameContext.pCamera;

	FixedCamera* pCamera = new FixedCamera();
	AddChild(pCamera);
	SetActiveCamera(pCamera->GetComponent<CameraComponent>());
	pCamera->GetTransform()->Translate(XMFLOAT3(-30.0f, 80.0f, -30.0f));
	pCamera->GetTransform()->Rotate(XMFLOAT3(20.0f, 45.0f, 0.0f));

	GameObject* obj = new GameObject();
	m_pTerrain = new GeneratedTerrainComponent(200, 200);
	obj->AddComponent(m_pTerrain);
	AddChild(obj);

	GameObject* pSky = new GameObject();
	ModelComponent* pMesh = new ModelComponent(L"./Resources/Meshes/box.ovm");
	pMesh->CastShadows(false);
	SkyboxMaterial* pMaterial = new SkyboxMaterial();
	pMaterial->SetTexture(L"./Resources/Textures/GS/Sky.dds");
	gameContext.pMaterialManager->AddMaterial(pMaterial, 0);
	pMesh->SetMaterial(0);
	pSky->AddComponent(pMesh);
	AddChild(pSky);

	//Sprites and fonts
	m_pButtonDefault = ContentManager::Load<TextureData>(L"./Resources/Textures/GS/ButtonDefault.png");
	m_pButtonHover = ContentManager::Load<TextureData>(L"./Resources/Textures/GS/ButtonHover.png");
	m_pCycleButtonDefault = ContentManager::Load<TextureData>(L"./Resources/Textures/GS/ButtonSmallDefault.png");
	m_pCycleButtonHover = ContentManager::Load<TextureData>(L"./Resources/Textures/GS/ButtonSmallHover.png");

	m_pSpriteFont = ContentManager::Load<SpriteFont>(L"./Resources/Fonts/Minecraft.fnt");

	//Input
	gameContext.pInput->AddInputAction(InputAction(0, Pressed, 'U'));

	//Buttons
	m_MidX = OverlordGame::GetGameSettings().Window.Width / 2.0f;
	m_MidY = OverlordGame::GetGameSettings().Window.Height / 2.0f;
	m_pButtonManager = new ButtonManager();
	m_pButtonManager->AddButton(new Button(XMFLOAT2(m_MidX, m_MidY * 2.0f - 60.0f), [this]() {if (m_RandomSeed) m_pTerrain->SetSeed(rand()); m_pTerrain->Generate(); }, m_pButtonDefault, m_pButtonHover));

	m_pButtonManager->AddButton(new Button(XMFLOAT2(30.0f, 50.0f), [this]() {m_pTerrain->SetLacunarity(m_pTerrain->GetLacunarity() - 0.1f); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));
	m_pButtonManager->AddButton(new Button(XMFLOAT2(250.0f, 50.0f), [this]() {m_pTerrain->SetLacunarity(m_pTerrain->GetLacunarity() + 0.1f); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));

	m_pButtonManager->AddButton(new Button(XMFLOAT2(30.0f, 120.0f), [this]() {m_pTerrain->SetPersistance(m_pTerrain->GetPersistance() - 0.1f); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));
	m_pButtonManager->AddButton(new Button(XMFLOAT2(250.0f, 120.0f), [this]() {m_pTerrain->SetPersistance(m_pTerrain->GetPersistance() + 0.1f); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));

	m_pButtonManager->AddButton(new Button(XMFLOAT2(30.0f, 190.0f), [this]() {m_pTerrain->SetOctaves(m_pTerrain->GetOctaves() - 1); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));
	m_pButtonManager->AddButton(new Button(XMFLOAT2(250.0f, 190.0f), [this]() {m_pTerrain->SetOctaves(m_pTerrain->GetOctaves() + 1); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));

	m_pButtonManager->AddButton(new Button(XMFLOAT2(30.0f, 260.0f), [this]() {m_pTerrain->SetScale(m_pTerrain->GetScale() - 5.0f); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));
	m_pButtonManager->AddButton(new Button(XMFLOAT2(250.0f, 260.0f), [this]() {m_pTerrain->SetScale(m_pTerrain->GetScale() + 5.0f); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));

	m_pButtonManager->AddButton(new Button(XMFLOAT2(30.0f, 330.0f), [this]() {m_RandomSeed = !m_RandomSeed; }, m_pCycleButtonDefault, m_pCycleButtonHover));
	m_pButtonManager->AddButton(new Button(XMFLOAT2(250.0f, 330.0f), [this]() {m_RandomSeed = !m_RandomSeed; }, m_pCycleButtonDefault, m_pCycleButtonHover));

	m_pButtonManager->AddButton(new Button(XMFLOAT2(30.0f, 400.0f), [this]() {m_pTerrain->SetSeed(m_pTerrain->GetSeed() - 1); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));
	m_pButtonManager->AddButton(new Button(XMFLOAT2(250.0f, 400.0f), [this]() {m_pTerrain->SetSeed(m_pTerrain->GetSeed() + 1); if (m_Dirty)m_pTerrain->Generate(); }, m_pCycleButtonDefault, m_pCycleButtonHover));

	m_pButtonManager->AddButton(new Button(XMFLOAT2(30.0f, 470.0f), [this]() {m_Dirty = !m_Dirty; }, m_pCycleButtonDefault, m_pCycleButtonHover));
	m_pButtonManager->AddButton(new Button(XMFLOAT2(250.0f, 470.0f), [this]() {m_Dirty = !m_Dirty; }, m_pCycleButtonDefault, m_pCycleButtonHover));

	m_pButtonManager->AddButton(new Button(XMFLOAT2(30.0f, 540.0f), [this]() {m_HalfLambert = !m_HalfLambert; m_pTerrain->SetTechnique(m_HalfLambert ? 1 : 0); }, m_pCycleButtonDefault, m_pCycleButtonHover));
	m_pButtonManager->AddButton(new Button(XMFLOAT2(250.0f, 540.0f), [this]() {m_HalfLambert = !m_HalfLambert; m_pTerrain->SetTechnique(m_HalfLambert ? 1 : 0);  }, m_pCycleButtonDefault, m_pCycleButtonHover));
}
void GeometryShaderScene::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	if (gameContext.pInput->IsActionTriggered(0))
	{
		m_InterfaceEnabled = !m_InterfaceEnabled;
		CameraComponent* currCam = gameContext.pCamera;
		m_pPrevCam->GetTransform()->Translate(currCam->GetTransform()->GetWorldPosition());
		XMVECTOR rot = XMLoadFloat4(&currCam->GetTransform()->GetRotation());
		m_pPrevCam->GetTransform()->Rotate(rot);
		SetActiveCamera(m_pPrevCam);
		m_pPrevCam = currCam;
	}

	if(m_InterfaceEnabled)
		m_pButtonManager->Update(gameContext);
}

void GeometryShaderScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	if (m_InterfaceEnabled)
	{
		m_pButtonManager->Draw();

		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"Generate", XMFLOAT2(m_MidX - 60.0f, m_MidY * 2.0f - 75.0f));

		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"Lacunarity", XMFLOAT2(60.0f, 20.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, to_wstring(m_pTerrain->GetLacunarity()), XMFLOAT2(60.0f, 50.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"<", XMFLOAT2(25.0f, 30.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L">", XMFLOAT2(240.0f, 30.0f));

		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"Persistance", XMFLOAT2(60.0f, 90.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, to_wstring(m_pTerrain->GetPersistance()), XMFLOAT2(60.0f, 120));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"<", XMFLOAT2(25.0f, 100.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L">", XMFLOAT2(240.0f, 100.0f));

		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"Octaves", XMFLOAT2(60.0f, 160.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, to_wstring(m_pTerrain->GetOctaves()), XMFLOAT2(60.0f, 190));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"<", XMFLOAT2(25.0f, 170.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L">", XMFLOAT2(240.0f, 170.0f));

		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"Scale", XMFLOAT2(60.0f, 230.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, to_wstring(m_pTerrain->GetScale()), XMFLOAT2(60.0f, 260));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"<", XMFLOAT2(25.0f, 240.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L">", XMFLOAT2(240.0f, 240.0f));

		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"Random Seed", XMFLOAT2(60.0f, 300.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, m_RandomSeed ? L"Yes" : L"No", XMFLOAT2(60.0f, 330));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"<", XMFLOAT2(25.0f, 310.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L">", XMFLOAT2(240.0f, 310.0f));

		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"Seed", XMFLOAT2(60.0f, 370.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, to_wstring(m_pTerrain->GetSeed()), XMFLOAT2(60.0f, 400));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"<", XMFLOAT2(25.0f, 380.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L">", XMFLOAT2(240.0f, 380.0f));

		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"Auto-Update", XMFLOAT2(60.0f, 440.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, m_Dirty ? L"Yes" : L"No", XMFLOAT2(60.0f, 470));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"<", XMFLOAT2(25.0f, 450.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L">", XMFLOAT2(240.0f, 450.0f));

		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"Shading model", XMFLOAT2(60.0f, 510.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, m_HalfLambert ? L"Half Lambert" : L"Lambert", XMFLOAT2(60.0f, 540));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L"<", XMFLOAT2(25.0f, 520.0f));
		TextRenderer::GetInstance()->DrawText(m_pSpriteFont, L">", XMFLOAT2(240.0f, 520.0f));
	}
}
