#include "stdafx.h"

#include "GeneratedTerrainComponent.h"
#include "SimplexNoise.h"

#include <Content/ContentManager.h>
#include <Scenegraph/GameObject.h>
#include <Scenegraph/GameScene.h>
#include <Components/TransformComponent.h>
#include <Graphics/TextureData.h>

ID3DX11EffectMatrixVariable* GeneratedTerrainComponent::m_pWorldVar = nullptr;
ID3DX11EffectMatrixVariable* GeneratedTerrainComponent::m_pWvpVar = nullptr;
ID3DX11EffectShaderResourceVariable* GeneratedTerrainComponent::m_pTextureVariable = nullptr;

GeneratedTerrainComponent::GeneratedTerrainComponent(int width, int depth):
	BaseComponent(),
	m_Capacity(width * depth),
	m_Width(width),
	m_Depth(depth)
{
}


GeneratedTerrainComponent::~GeneratedTerrainComponent(void)
{
	SafeRelease(m_pInputLayout);
	SafeRelease(m_pVertexBuffer);
}

void GeneratedTerrainComponent::Initialize(const GameContext& gameContext)
{
	LoadEffect(gameContext);
	CreateInputLayout(gameContext, m_pTechnique);
	CreateVertexBuffer(gameContext);
	Generate();
}

void GeneratedTerrainComponent::Generate()
{
	//Clear the noisemap to create new one
	m_Vertices.clear();
	m_Vertices.resize(m_Width * m_Depth);

	//With seed, calculate random sample offsets for each octave
	srand(m_Seed);
	vector<XMFLOAT2> randomOffsets;
	randomOffsets.resize(m_Octaves);
	for (int i = 0; i < m_Octaves; i++)
	{
		randomOffsets[i].x = static_cast<float>(rand() % 200000 - 100000);
		randomOffsets[i].y = static_cast<float>(rand() % 200000 - 100000);
	}

	float minHeight = numeric_limits<float>::max();
	float maxHeight = numeric_limits<float>::min();

	for (int y = 0; y < m_Depth; y++)
	{
		for (int x = 0; x < m_Width; x++)
		{
			float amplitude = 1.0f;
			float frequency = 1.0f;
			float sumHeight = 0.0f;

			for (int o = 0; o < m_Octaves; o++)
			{
				float sampleX = static_cast<float>(x - m_Width / 2.0f) / m_Scale * frequency + randomOffsets[o].x;
				float sampleY = static_cast<float>(y - m_Depth / 2.0f) / m_Scale * frequency + randomOffsets[o].y;

				float value = SimplexNoise::noise(sampleX, sampleY);
				sumHeight += value * amplitude;

				amplitude *= m_Persistance;
				frequency *= m_Lacunarity;
			}
			//inverse lerp the sumHeight
			if (sumHeight < minHeight)
				minHeight = sumHeight;
			else if (sumHeight > maxHeight)
				maxHeight = sumHeight;

			m_Vertices[x + y * m_Width].y = sumHeight;
		}
	}

	//Have the values between 0 and 1
	for (int y = 0; y < m_Depth; y++)
	{
		for (int x = 0; x < m_Width; x++)
		{
			int idx = x + y * m_Width;
			m_Vertices[idx].y = InverseLerp(minHeight, maxHeight, m_Vertices[idx].y);
			m_Vertices[idx].x = static_cast<float>(x);
			m_Vertices[idx].z = static_cast<float>(y);
		}
	}
	UpdateBuffer();
}

void GeneratedTerrainComponent::LoadEffect(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pEffect = ContentManager::Load<ID3DX11Effect>(L"./Resources/Effects/TerrainGenerator.fx");
	//Get the first technique
	m_pTechnique = m_pEffect->GetTechniqueByIndex(1);

	//World matrix
	m_pWorldVar = m_pEffect->GetVariableBySemantic("WORLD")->AsMatrix();
	if (!m_pWorldVar->IsValid())
		Logger::LogWarning(L"GeneratedTerrainComponent: WORLD variable not valid!");

	//WVP
	m_pWvpVar = m_pEffect->GetVariableBySemantic("WORLDVIEWPROJECTION")->AsMatrix();
	if (!m_pWvpVar->IsValid())
		Logger::LogWarning(L"GeneratedTerrainComponent: WORLDVIEWPROJECTION variable not valid!");

	//Texture Atlas
	m_pTextureVariable = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
	if (!m_pTextureVariable->IsValid())
		Logger::LogWarning(L"GeneratedTerrainComponent: gTexture variable not valid!");
	TextureData* pTexture = ContentManager::Load<TextureData>(L"./Resources/Textures/GS/TerrainTypes.png");
	m_pTextureVariable->SetResource(pTexture->GetShaderResourceView());
}

void GeneratedTerrainComponent::CreateVertexBuffer(const GameContext& gameContext)
{
	if (m_pVertexBuffer)
		SafeRelease(m_pVertexBuffer);

	D3D11_BUFFER_DESC vertexBuffDesc;
	vertexBuffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBuffDesc.ByteWidth = sizeof(XMFLOAT3) * m_Capacity;
	vertexBuffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexBuffDesc.Usage = D3D11_USAGE_DYNAMIC;
	vertexBuffDesc.MiscFlags = 0;

	auto hr = gameContext.pDevice->CreateBuffer(&vertexBuffDesc, NULL, &m_pVertexBuffer);
	if (Logger::LogHResult(hr, L"GeneratedTerrainComponent::CreateVertexBuffer"))
		return;
}

void GeneratedTerrainComponent::CreateInputLayout(const GameContext& gameContext, ID3DX11EffectTechnique* pTechnique)
{
	UNREFERENCED_PARAMETER(pTechnique);
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	D3DX11_PASS_DESC PassDesc;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&PassDesc);
	auto result = gameContext.pDevice->CreateInputLayout(layout, 1, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_pInputLayout);
	Logger::LogHResult(result, L"GeneratedTerrainComponent::LoadEffect(...)");
}

void GeneratedTerrainComponent::UpdateBuffer()
{
	GameContext gameContext = m_pGameObject->GetScene()->GetGameContext();
	size_t size = m_Vertices.size();
	if (size == 0)return;
	if (size > m_Capacity)
	{
		Logger::LogInfo(L"GeneratedTerrainComponent::UpdateBuffer > Buffer size too small!");
		size = m_Capacity;
	}

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	gameContext.pDeviceContext->Map(m_pVertexBuffer, 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &mappedResource);
	memcpy(mappedResource.pData, m_Vertices.data(), sizeof(XMFLOAT3) * size);
	gameContext.pDeviceContext->Unmap(m_pVertexBuffer, 0);
}

void GeneratedTerrainComponent::Update(const GameContext& gameContext)
{
	//Update the shader variables
	auto world = XMLoadFloat4x4(&GetTransform()->GetWorld());
	auto viewProjection = XMLoadFloat4x4(&gameContext.pCamera->GetViewProjection());

	m_pWorldVar->SetMatrix(reinterpret_cast<float*>(&world));

	XMMATRIX wvp = world * viewProjection;
	m_pWvpVar->SetMatrix(reinterpret_cast<float*>(&wvp));
}

void GeneratedTerrainComponent::Draw(const GameContext& gameContext)
{
	gameContext.pDeviceContext->IASetInputLayout(m_pInputLayout);
	UINT offset = 0;
	UINT stride = sizeof(XMFLOAT3);
	gameContext.pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	//POINTLIST
	gameContext.pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	D3DX11_TECHNIQUE_DESC techDesc;
	m_pTechnique->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p) 
	{
		m_pTechnique->GetPassByIndex(p)->Apply(0, gameContext.pDeviceContext);
		gameContext.pDeviceContext->Draw(m_Vertices.size(), 0);
	}
}