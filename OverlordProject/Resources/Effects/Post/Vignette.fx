Texture2D gTexture;

float4 gColor;
float gRadius = 0.75f;
float gSmoothness = 0.25f;
float2 gCenter = float2(0.5f, 0.5f);

SamplerState samLinear
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
};

DepthStencilState DepthEnabled
{
	DepthEnable = TRUE;
	DepthWriteMask = 1;
};

RasterizerState BackfaceCulling
{
	CullMode = BACK;
};

struct VS_INPUT
{
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD1;
};

PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;
	output.Position = float4(input.Position, 1.0f);
	output.TexCoord = input.TexCoord;

	return output;
}

float4 PS(PS_INPUT input) : SV_TARGET
{
	float2 pos = input.TexCoord - float2(gCenter.x, gCenter.y);
	float length = sqrt(pos.x * pos.x + pos.y * pos.y);
	float value = smoothstep(gRadius, gSmoothness, length);

	float4 vignette = (1 - value) * gColor;
	float4 diffuse = gTexture.Sample(samLinear, input.TexCoord);
	float4 final = saturate(vignette + diffuse);
	return final;
}

technique11 EdgeDetection
{
	pass P0
    {
		SetDepthStencilState(DepthEnabled, 0);
		SetRasterizerState(BackfaceCulling);
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}