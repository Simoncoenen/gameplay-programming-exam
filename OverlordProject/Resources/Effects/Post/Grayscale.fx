//=============================================================================
//// Shader uses position and texture
//=============================================================================

float gStrength = 1.0f;

SamplerState samPoint
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Mirror;
    AddressV = Mirror;
};
SamplerState samLinear
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;// or Mirror or Clamp or Border
    AddressV = Wrap;// or Mirror or Clamp or Border
};

Texture2D gTexture;

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
/// Create Rasterizer State (Backface culling) 
DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};

RasterizerState BackfaceCulling
{
	CullMode = BACK;
};

//IN/OUT STRUCTS
//--------------
struct VS_INPUT
{
    float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;

};

struct PS_INPUT
{
    float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD1;
};


//VERTEX SHADER
//-------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;
	// Set the Position
    output.Position = float4(input.Position, 1.0);
	// Set the TexCoord
    output.TexCoord = input.TexCoord;
	
	return output;
}


//PIXEL SHADER
//------------
float4 PS_Average(PS_INPUT input): SV_Target
{
    float3 color = gTexture.Sample(samLinear, input.TexCoord);
    float mean = (color.x,color.y,color.z)/3.0;
	
    return float4( mean, mean, mean, 1.0f );
}

float4 PS_Luminosity(PS_INPUT input): SV_Target
{
	float3 color = gTexture.Sample(samLinear, input.TexCoord);
	float output = 0.3f * color.r + 0.59f * color.g + 0.11f * color.b;

	float red = lerp(color.r, output, gStrength);
	float green = lerp(color.g, output, gStrength);
	float blue = lerp(color.b, output, gStrength);

    return float4( red, green, blue, 1.0f );
}


//TECHNIQUE
//---------
technique11 GrayscaleAverage
{
    pass P0
    {          
        SetRasterizerState(BackfaceCulling);
		SetDepthStencilState(EnableDepth, 0);

		SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS_Average() ) );
    }
}

technique11 GrayscaleLuminosity
{
    pass P0
    {          
        SetRasterizerState(BackfaceCulling);
		SetDepthStencilState(EnableDepth, 0);

		SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS_Luminosity() ) );
    }
}
